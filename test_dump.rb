Owner.create!([
  {sho_code: "1111", shimei: nil, jusho: nil},
  {sho_code: "1111", shimei: "漢字　名前 aの下", jusho: "愛知県新城市浅谷\r\n"},
  {sho_code: "1111", shimei: "漢字　名前", jusho: "愛知県新城市\r\n"},
  {sho_code: "2222", shimei: "漢字　名前", jusho: "愛知県刈谷市\r\n"},
  {sho_code: "12345", shimei: "漢字　名前", jusho: "愛知県安城\r\n"},
  {sho_code: "12345", shimei: "漢字　名前", jusho: "愛知県安城\r\n"},
  {sho_code: "1111", shimei: "漢字　名前3", jusho: "愛知県新城市\r\n"}
])
