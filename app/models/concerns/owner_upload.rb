module OwnerUpload

def ownerUp
  data = []
  File.foreach(Rails.root.to_s + "/tmp/owners.txt") do |line|
    data.push line.split(/\t/)
  end
  
  chk = 0
  data.each do |a|
    if a[0][/^\d{4}/]
      chk = 0 
    else
      chk += 1
    end
    (print "Too many address lines!\n";exit) if chk > 1
  end
  
  #The 1st line is title
  1.upto(data.size - 1) do |n|
    next if data[n][0][/^\n/]
    unless data[n][0][/^\d{4}/]
      jusho = data[n - 1][2] + "&&" + data[n][0].gsub("　","")
      data[n - 1][2] = jusho.gsub(/[\"\n]/,"")
      data[n - 1].push data[n][1]
    end
  end
  data.each do |a| a[-1].chop! end
  data.each do |a|
    next if not a[0][/^\d{4}/]
    h = Hash[[:sho_code, :shimei, :jusho, :note].zip a]
    h[:note].gsub!("　","/") if h[:note]
    h[:note].gsub!(" ","/") if h[:note]
    if owner = Owner.find_by( sho_code: h[:sho_code])
      owner.update( h )
    else
      owner = Owner.new
      owner.attributes = h
      owner.save!
    end
  end
  nil
end





end #end of module