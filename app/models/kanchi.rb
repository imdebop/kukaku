class Kanchi < ActiveRecord::Base
#  belongs_to :owner, foreign_key: "sho_code"
  self.primary_key = "kanchi_code"
  belongs_to :land
  has_many :kan_hyokas, foreign_key: "kanchi_code"
end
