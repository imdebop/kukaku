#class KihonCode < ActiveRecord::Base
class KihonCode
  @@h2_code = Hash.new
  arr = IO.readlines(Rails.root + "app/models/code_table.txt", rs = $/)
  arr.each do |str|
    kubun,code,value = str.chop.split(/:/)
    next if kubun.nil?
    if @@h2_code[kubun].nil?
      @@h2_code[kubun]={}
      @@h2_code[kubun][code] = value
    else
      @@h2_code[kubun][code] = value
    end
  end
  @@h2_code.keys.each do |key| @@h2_code[key][nil] = "** Nil key! **" end

  def self.fcode_to_shozai f_code
    if f_code[/^\d{7}$/]
      code = "0" + f_code
    else
      code = f_code
    end
    cho, aza, chiban = code.unpack("a2a3a3")
    chomei = @@h2_code["choaza"][cho]
    str = format("%s %d",chomei, aza.to_i)
    str += format("-%d", chiban.to_i) if chiban != "000"
    str
  end

  def self.sokuhai keitai
    case keitai
      when '+' then ["soku", 0.5]
      when 'T' then ["soku", 0.5]
      when 'L' then ["soku", 0.25]
      when '=' then ["hai", 0.075]
      when '-' then ["hai", 0.04]
    end
  end

  def self.getVal
    return @@h2_code
  end

  def self.zosin
    1.006
  end

  def self.maguchi len
    x = 1.0
    l = len.to_f
    if l < 2.0
      x = 0.8
    elsif l < 2.5
      x = 0.84
    elsif l < 3.0
      x = 0.88
    elsif l < 3.5
      x = 0.92
    elsif l < 4.0
      x = 0.96
    end
    return format("%4.2f",x)
  end

  def self.chodai rate
    x = 0.90
    r = rate.to_f
    if r < 3.0
      x = 1.00
    elsif r < 4.0
      x = 0.99
    elsif r < 5.0
      x = 0.98
    elsif r < 6.0
      x = 0.97
    elsif r < 7.0
      x = 0.96
    elsif r < 8.0
      x = 0.94
    elsif r < 9.0
      x = 0.92
    end
    return format("%4.2f",x)
  end

  def self.sankaku kaku_rate
    kaku, rate = format("%06.2f",kaku_rate.to_f).split(/\./)
    if kaku.to_i < 20
      k = "1U"
    elsif kaku.to_i > 84
      return 1.0
    elsif rate.to_i < 5
      return 1.0
    elsif kaku[/..[0-4]/]
      k = kaku[1,1] + "D"
    else
      k = kaku[1,1] + "U"
    end
    if rate == '00'
      r = '10'
    else
      r = '0' + rate[0,1]
    end
    str = k + r
    res = @@h2_code['sankaku'][str]
    if res.nil?
      res = "no result for:" + kaku_rate
      p res
    end
    return res
  end

  def self.kotei takasa
    x = 0.000 #エラーの意味 yet implemented
    if takasa < 0
      x = self.kotei_2(takasa)
    else
      if takasa < 1; return 1.000
        elsif takasa < 1.5; return 0.999
        elsif takasa < 2.0; return 0.995
        elsif takasa < 2.5; return 0.990
        elsif takasa < 3.0; return 0.984
        elsif takasa < 3.5; return 0.976
        elsif takasa < 4.0; return 0.967
      end
    end
    #return format("%4.3f",x)
    return x
  end

  def self.kotei_2 takasa
    x = 0.000
    t = takasa.abs
    if t < 0.3; return 1.000
      elsif t < 0.5; return 0.993
      elsif t < 1.0; return 0.978
      elsif t < 1.5; return 0.964
      elsif t < 2.0; return 0.949
      elsif t < 2.5; return 0.934
      elsif t < 3.0; return 0.920
      elsif t < 3.5; return 0.905
      elsif t < 4.0; return 0.890
    end
    return x #エラーの意味 yet implemented
  end
end
