class Owner < ActiveRecord::Base
  self.primary_key = "sho_code"
  has_many :lands, foreign_key: "sho_code"
#  belongs_to :land
end
