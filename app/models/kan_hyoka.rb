class KanHyoka < ActiveRecord::Base
#  belongs_to :owner, foreign_key: "sho_code"
  self.primary_key = "kanchi_code"
  belongs_to :kanchi
#end
#class KanHyoka
  shusei_str = "00奥行:01方位:02間口:03長大:04三角等:05水路" +
              ":06高水:07高低:08島地:09袋地"
  @@shusei_tbl = shusei_str.split(/:/).map do |a| a[/^(..)(.+)$/];[$1,$2] end

  rec_shusei_str = "奥行00:方位01:間口02:長大03:三角04:水路05" +
                  ":高・06:高低07:島地08:袋地09"
  @@rec_shusei_tbl =
      rec_shusei_str.split(/:/).map do |a| a[/^(..)(..)$/];[$1,$2]  end

  @@h_arr_to_text={}

  rec_shusei_str.split(/:/).each do |a| a[/^(..).+$/]
    @@h_arr_to_text[$1] = Proc.new{|item, val| item + ":#{val}\n"}
  end
  @@h_arr_to_text["奥行"]=Proc.new{|item, val|
    "奥行:#{item[2..-1]}\n"
  }
  @@h_arr_to_text["間口"]=Proc.new{|item, val|
    "間口:#{item[2..-1]}\n"
  }
  @@h_arr_to_text["三角"]=Proc.new{|item, val|
    #cf.三角等56.34:x.xxx
    "三角:#{item[2..-1]}\n"
  }
  @@h_arr_to_text["高・"]=Proc.new{|item, val|
    #cf.高・水1
    "高水:#{val}\n"
  }
  @@h_arr_to_text["高道"]=Proc.new{|item, val|
    #cf.高・水1
    "高水:#{val}\n"
  }
  @@h_arr_to_text["長大"]=Proc.new{|item, val|
    "長大:#{item[2..-1]}\n"
  }
  @@h_arr_to_text["方位"]=Proc.new{|item, val|
    "方位:#{item[/[A-Z]+$/]}\n"
  }
  #20191206
  @@h_arr_to_text["奥単"]=Proc.new{|item, val|
    "袋地:#{item[2..-1].to_i.to_s}\n"
  }
  #20210116
  @@h_arr_to_text["高低"]=Proc.new{|item, val|
    "高低:#{item[2..-1]}\n"
  }


  def self.shusei_arr_to_text arr_shusei
p "**** arr_shusei DUMP ---kan_hyoka.rb---- *******"
pp arr_shusei
    str = ""
    arr_shusei.each do |arr|
      item, val = arr
      key = item[0,2]
      next if key == "袋地"  #200191206
      if pr = @@h_arr_to_text[key]
        str += pr.call(item, val)
      else
        str += "*** shusei key not found! ***\n"
        p arr
      end
    end
p str
    str
  end

#  def self.shusei_arr_to_hash arr_shusei
#    h = {}
#    arr_shusei.each do |rec|
#      if rec[0][/^奥行/]
#        h[:oku] = [rec[0][/[0-9]+$].to_f, rec[1]]
#      elsif rec[0][/^方向/]
#        h[:hoko] = [rec[0][/[A-Z]+$/], rec[1]]
#      end
#    end
#  end

  def self.sokuhai_arr_to_text arr_soku
    str = ""
#pp arr_soku
    return str if arr_soku.size == 0
    str = ''
    arr_soku.each do |a|
      str += "->側面・背面加算--\n"
      str += "  形態    :#{a.keitai}\n"
      str += "  路線番号:#{a.rosen}\n"
      str += "  延長    :#{a.len}\n"

    end
    str
  end

end
