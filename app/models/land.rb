class Land < ActiveRecord::Base
  require 'csv'

  self.primary_key = "f_code"
  has_many :children, class_name: "Land",
                      foreign_key: "f_code_parent"
  has_many :kanchis, foreign_key: "f_code"
#  has_one :owner, foreign_key: "sho_code"
  belongs_to :owner

  attr_accessor :horyu
  def initialize
    @horyu = nil
  end

  def self.import( file )
    CSV.foreach( file.path, headers: true ) do |row|
#pp row;exit
      h_rec = row.to_hash.slice(*updatable_attributes)
p h_rec
      fcode = h_rec["f_code"]
      next if fcode.nil?
      h_rec["f_code"] = "0" + fcode if fcode.size == 7
      scode = h_rec["sho_code"]
      h_rec["sho_code"] = format("%04d",scode.to_i) if scode.size < 4
      h_rec["kenri_sisu"] = (row["hyotei_sisu"].to_f * 1.006).round
      if land = find_by( f_code: h_rec["f_code"])
#        land.attributes = h_rec
        land.update(h_rec)
      else
#        next if h_rec[ "f_code" ].nil?
        land = new
        land.attributes = h_rec
        land.save!
      end
    end
    File.delete(file.path)
  end

  def self.updatable_attributes
    ["f_code", "toki_chimoku", "toki_men", "kijun_men", "zen_m2sisu", "hyotei_sisu", "sho_code"]
  end

end
