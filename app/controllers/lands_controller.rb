class LandsController < ApplicationController

  def show
    @lands = Land.where( sho_code: params[:id] )
  end

  def new
    @land = Land.new
  end

  def edit
    @land = Land.find(params[:id])
  end

  def index
    if sho = params[:sho_code]
      @lands = Land.where( sho_code: sho)
    else
      @lands = Land.all
    end
  end

  def create
    @land = Land.new(land_params)
 
    @land.save
#    redirect_to @land
p "route create->save"
    redirect_to land_path( id: @land.sho_code )
  end

  def update
    @land = Land.find(params[:id])

    if @land.update(land_params)
      view_context.recalc @land
      redirect_to action: 'index'
    else
      render 'edit'
    end
  end

  def destroy
    @land = Land.find(params[:id])
    @land.destroy
 
    redirect_to lands_path
  end
  
  def import
    Land.import(params[:file])
    redirect_to lands_path
  end

  def kenri
    @land = Land.find(params[:id])
    @land.update(kenri_sisu: (@land.hyotei_sisu.to_f * 1.006).round )
    redirect_to lands_path
  end

  private
    def land_params
      params.require(:land).permit(:f_code, :f_code_parent, :toki_chimoku,
      :toki_men, :zen_m2sisu, :kijun_men,:sho_code)
    end
end
