class OwnersController < ApplicationController
  def index
    @owners = Owner.all
  end

  def show
    @owner = Owner.find(params[:id])
  end

  def new
  end

  def edit
    @owner = Owner.find(params[:id])
  end

  def create
    @owner = Owner.new(owner_params)
 
    @owner.save
    redirect_to @owner
  end

  def update
    @owner = Owner.find(params[:id])
 
    if @owner.update(owner_params)
      redirect_to @owner
    else
      render 'edit'
    end
  end

  def save2cvs
    render params[ :owner ].inspect
  end

  def destroy
    @owner = Owner.find(params[:id])
    @owner.destroy
 
    redirect_to owners_path
  end
  private
    def owner_params
      params.require(:owner).permit(:sho_code, :shimei, :jusho)
    end
end
