class KanHyokasController < ApplicationController
  def show
    @kan_hyoka = KanHyoka.find(params[:id])
  end

  def new
    @kan_hyoka = KanHyoka.new
  end

  def edit
    @kan_hyoka = KanHyoka.find(params[:id])
  end

  def index
    @kan_hyokas = KanHyoka.all
  end

  def create
    @kan_hyoka = KanHyoka.new(kan_hyoka_params)
 
    @kan_hyoka.save
    redirect_to @kan_hyoka
 
  end

  def update
    @kan_hyoka = KanHyoka.find(params[:id])
 
    if @kan_hyoka.update(kan_hyoka_params)
      redirect_to @kan_hyoka
    else
      render 'edit'
    end
  end


  def destroy
    @kan_hyoka = KanHyoka.find(params[:id])
    @kan_hyoka.destroy
 
    redirect_to kan_hyokas_path
  end

  private
    def kan_hyoka_params
      params.require(:kan_hyoka).permit(:kanchi_code, :fugo, :men, :sisu)
    end
end
