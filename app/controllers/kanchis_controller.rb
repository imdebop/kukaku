class KanchisController < ApplicationController
  include KanchisHelper
  
  def show
    @kanchi = Kanchi.find(params[:id])
  end

  def new
    @kanchi = Kanchi.new
  end

  def edit
    @kanchi = Kanchi.find(params[:id])
  end

  def index
    @kanchis = Kanchi.order( 'updated_at DESC')
  end

  def create
   @kanchi = Kanchi.new(kanchi_params)
 
    @kanchi.save
    redirect_to @kanchi
 
  end

  def upload
    @kanchi = Kanchi.new
#    @kanchi.kanchi_code = "test_code"
#    @kanchi.save
  end

  def upload_save
#    render plain: upload_params[ :tmp_text ].inspect
    @kanchi = Kanchi.new
    arrRec = nil
    res = save_jikko( @kanchi, upload_params[ :tmp_text ], arrRec )
    #render plain: res.to_yaml
    redirect_to kanchis_path
  end

  def update
    @kanchi = Kanchi.find(params[:id])
 
    if @kanchi.update(kanchi_params)
      redirect_to @kanchi
    else
      render 'edit'
    end
  end


  def destroy
    @kanchi = Kanchi.find(params[:id])
    @kanchi.destroy
 
    redirect_to kanchis_path
  end

  private
    def kanchi_params
      params.require(:kanchi).permit(:kanchi_code, :f_code, :kenri_men, :kanchi_men, :kan_m2sisu)
    end
    def upload_params
      params.require(:kanchi).permit(:kanchi_code, :tmp_text)
    end

end
