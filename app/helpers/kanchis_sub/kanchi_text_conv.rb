require 'bigdecimal'
module KanchiTextConv
  def text_conv details
    arr_zen = []
    arr = details.gsub(/\r/,"").split(/<>/); arr.shift
    zen, *arr_kan = arr
    arr_zen = zen_edit zen
    arr_oneRec = arr_kan.map do |kan| oneRec_edit(arr_zen, kan) end
p    msg = conv_keisan( arr_oneRec )
    [arr_oneRec, msg]
  end

  def zen_edit zen
    arr = []
    zen.split(/\n/).each do |a|
      next unless a[/^筆コード:(\w+)$/]
      zenRec = ZenRecKan.new
      zenRec.fudecode = fcode = $1
      zenRec.tokiChimoku = ''
      if fcode[/^h/] or fcode[/^50/]
        zenRec.sho_code    = '5000'
        zenRec.shimei      = '保留地'
       else
p fcode
        chomei, chiban = KihonCode.fcode_to_shozai( fcode ).split(/ /)
        zenRec.chomei = chomei; zenRec.chiban = chiban
        land = Land.find_by( f_code: zenRec.fudecode )
        zenRec.tokiChimoku = land.toki_chimoku
        zenRec.genkyoChimoku = land.genkyo_chimoku
        zenRec.tokiMen     = land.toki_men
        zenRec.kijunMen    = land.kijun_men
        zenRec.sho_code    = land.sho_code
        zenRec.shimei      = Owner.find_by(sho_code: land.sho_code).shimei
        zenRec.heibeiSisu  = land.zen_m2sisu
        zenRec.hyokaSisu   = land.hyotei_sisu
        zenRec.kenriSisu   = land.kenri_sisu
      end
      arr.push zenRec
    end
    arr
  end

  def oneRec_edit arr_zenRec, kan
    oneRec = OneUnitRec.new
    oneRec.arrZenRec = arr_zenRec
    kihon, *arr_hyoka = kan.split(/=>/)
    kihon[/^換地符号:(\w)/]
    bun = $1 ? $1 : ""
    oneRec.sort_key = arr_zenRec[0].fudecode + bun
    oneRec.h_HyokaRec, h_shusei, men = edit_hyoRec arr_hyoka
    oneRec.gokeiRec = edit_gokei(oneRec, kihon)
    oneRec.gokeiRec.kan_men = men.to_s
    conv_keisan_one(oneRec, h_shusei)
    oneRec
  end

  def edit_hyoRec arr_hyoka
    h = {}
    h_shusei = {}
    wk_hyo_men = 0
    arr_hyoka.each do |a|
      hyRec = HyokaRec.new
      hy, *sokuhai = a.split(/->/)
      arr = hy.split(/\n/).map do |a| a.split(/:/) end
      fugo = arr.assoc("評価符号")[1][0,1]
      hyRec.fugo = fugo == '-' ? '' : fugo
      hyRec.men = arr.assoc("評価地積").pop.strip
      wk_hyo_men += hyRec.men.to_f
      hyRec.rosen = arr.assoc("路線番号").pop.strip
      hyRec.rosenka = KihonCode.getVal["rosen_go"][hyRec.rosen]
      hyRec.arr_shusei, h_shusei = conv_shusei( arr )
      h_shusei[:rosen_go] = hyRec.rosenka
      hyRec.arr_soku = conv_soku_keisan( h_shusei, sokuhai )
      h[hyRec.fugo] = hyRec
    end
    [h, h_shusei, wk_hyo_men]
  end

  def conv_shusei data_arr
    arr2 = []
    h = {}
    conv_shusei_oku arr2, h, data_arr
    houi = data_arr.assoc("方位").pop.strip
    houi_rate = KihonCode.getVal["hoko"][houi]
    arr2.push [("方位 " + houi ), houi_rate ]
    h[ :houi ] = [houi, houi_rate]
    len = nil
    dum, len = data_arr.assoc("間口")
    if len
      mag_rate = format("%4.2f", KihonCode.maguchi(len) )
      arr2.push [("間口" + len) , mag_rate ]
      h[ :maguchi ] = [len, mag_rate.to_f ]
    else
      h[ :maguchi ] = nil
    end
    dum, kaku_rate = data_arr.assoc("三角")
#p kaku_rate
    if kaku_rate
      san_rate = format("%5.3f", KihonCode.sankaku(kaku_rate) )
      arr2.push [("三角" + kaku_rate) , san_rate ]
      h[ :sankaku ] = [kaku_rate, san_rate.to_f ]
    else
      h[ :sankaku ] = nil
    end
    dum, rate = data_arr.assoc("長大")
    if rate
      cho_rate = format("%4.2f", KihonCode.chodai(rate) )
      arr2.push [("長大" + rate) , cho_rate ]
      h[ :chodai ] = [len, cho_rate.to_f ]
    else
      h[ :chodai ] = nil
    end
    dum, rate = data_arr.assoc("高水")
    if rate
      arr2.push ["高道水敷" , rate ]
      h[ :koatu ] = ["", rate.to_f ]
    else
      h[ :koatu ] = nil
    end
    #20210116
    dum, takasa = data_arr.assoc("高低")
    if takasa
      k_rate = format("%4.3f",KihonCode.kotei(takasa.to_f))
      arr2.push [("高低"+takasa), k_rate]
      h[ :kotei ] = [takasa, k_rate.to_f ]
    else
      h[ :kotei ] = nil
    end

    [arr2, h]
  end

  def conv_shusei_oku arr2, h, data_arr
    dum, len = data_arr.assoc("袋地")
    if len
      len = format("%03d",len.to_i)
      oku_rate = format("%4.3f",KihonCode.getVal["tan"][len].to_f)
      arr2.push ["袋地" ,  "0.950"]
      h[ :fukuro ] = [len, 0.95 ]
      arr2.push [("奥単" + len) , oku_rate ]
      h[ :oku ] = [len, oku_rate.to_f ]
      return
    end
    dum, len = data_arr.assoc("奥行")
    len = format("%03d",len.to_i)
    oku_rate = format("%4.3f",KihonCode.getVal["oku"][len].to_f)
    arr2.push [("奥行" + len) , oku_rate ]
    h[ :oku ] = [len, oku_rate.to_f ]
  end

  def conv_soku_keisan h_shusei, arr_sokuhai
    arr = []
    arr_shusei = []
    arr_sokuhai.each do |rec|
      dum, *arr_wk = rec.gsub(" ","").chop.split(/\n/)
      arr_wk2 = arr_wk.map do |a| a.split(/:/) end
      sh = SokuHaiRec.new
      sh.keitai = arr_wk2.assoc("形態").pop
      sh.rosen = arr_wk2.assoc("路線番号").pop
      sh.rosenka = KihonCode.getVal["rosen_go"][sh.rosen]
      #sh_rosenka = sh.rosenka.to_f  20201012
      sh_rosenka = BigDecimal(sh.rosenka)
      sh.len = arr_wk2.assoc("延長").pop
      shurui, sh_rate = KihonCode.sokuhai sh.keitai
      sh_rateB = sh_rate.to_d
      if shurui == "soku"
       if h_shusei[:maguchi]
          maguchi, rate = h_shusei[:maguchi]
          maguchiB = BigDecimal(maguchi)
          #sh_rateB = BigDecimal(sh_rate)
          if maguchiB < 15.0
            x = sh_rateB * maguchiB / 15.0
            sh_rateB = x.round(3)
          end
        end
        sh.kasanR = format("%3.3f", sh_rateB)
        #sh.hyoka_sisuB = (sh_rosenka * sh_rate.to_f * sh.len.to_f).round
        sh.hyoka_sisuB = (sh_rosenka * sh_rateB * BigDecimal(sh.len)).round
      else
        shomen = h_shusei[:rosen_go].to_f
        okuyuki, oku_rate = h_shusei[ :oku ]
        if okuyuki.to_f < 30.0
          sh.kasanR = format("%3.3f", (sh_rate / 30.0 * okuyuki.to_f).round(3) )
        else
          sh.kasanR = "0.075"
        end
        sh.hyoka_sisuB = ( sh_rosenka * oku_rate.to_f * sh.kasanR.to_f *
            sh.len.to_f * okuyuki.to_f * (sh_rosenka/shomen)**2).round
      end
      arr.push( sh )
    end
    arr
  end

  def edit_gokei oneRec, kihon
    aZ = oneRec.arrZenRec
    gokeiRec = KanGokeiRec.new
    arr = kihon.split(/\n/).map{ |a| a.split(/:/) }
    gokeiRec.block_num = arr.assoc("街区").pop
    gokeiRec.lot_num = arr.assoc("画地").pop
    if wk = arr.assoc("検討")
      gokeiRec.kento = wk.pop
    else
      gokeiRec.kento = ""
    end
    arr_mem = []
    arr.each do |a|
      arr_mem.push a[1] if a[0] == 'メモ'
    end
    gokeiRec.arrMemo = arr_mem
    horyu_code(gokeiRec.block_num, gokeiRec.lot_num)
    if aZ[0].fudecode[/^h/] or aZ[0].fudecode[/^50/]
      code = horyu_code(gokeiRec.block_num, gokeiRec.lot_num)
      aZ[0].fudecode = code
      oneRec.sort_key = code
      return gokeiRec
    end

    init_gokei gokeiRec
    if bun_men = arr.assoc("分割面積")
      gokeiRec.toki_men = ""
      gokeiRec.kijun_men = bun_men[1].to_f
      gokeiRec.hyotei_sisu = ""
    else
      aZ.each do |zen| add_gokei(gokeiRec, zen) end
    end

    gokeiRec
  end

  def init_gokei gokeiRec
    gokeiRec.toki_men = 0
    gokeiRec.kijun_men = 0
    gokeiRec.hyotei_sisu = 0
    gokeiRec.kenri_sisu = 0
  end

  def add_gokei gokeiRec, zen
    gokeiRec.toki_men += zen.tokiMen
    gokeiRec.kijun_men += zen.kijunMen
    gokeiRec.hyotei_sisu += zen.hyokaSisu
    gokeiRec.kenri_sisu += zen.kenriSisu.to_i
  end

  def conv_keisan arr_oneRec
    if arr_oneRec.size > 1
      kijun_men = arr_oneRec[0].arrZenRec[0].kijunMen.to_f
    else
      kijun_men = arr_oneRec[0].gokeiRec.kijun_men.to_f
    end
    msg = nil
    if arr_oneRec.size > 1
      msg = kijun_men_zan arr_oneRec
    else
      msg = nil
      kenri_men_cal arr_oneRec[0]
    end
    fcode = arr_oneRec[0].arrZenRec[0].fudecode
    if arr_oneRec.size == 1
      r = arr_oneRec[0]
      h_1n = r.gokeiRec.h_1n = {}
      h_1n[fcode] = ['', '', '']
      kenri_s = r.gokeiRec.kenri_men.to_f
      kanchi_s = r.gokeiRec.kan_men.to_f
      kenri_genbuR,kanchi_genbuR = gokei_genbu_cal(kenri_s, kanchi_s, kijun_men)
      r.gokeiRec.kenri_genbuR = kenri_genbuR
      r.gokeiRec.kan_genbuR = kanchi_genbuR
      gokei_seisan r
    else
      arr_kakuchi = []
      kenri_men_sum = 0.0
      kanchi_men_sum = 0.0
      arr_oneRec.each do |oneR|
        h_1n = oneR.gokeiRec.h_1n = {}
        fugo = oneR.sort_key[-1, 1]
        key = fcode + fugo
        h_1n[key] = [oneR.gokeiRec.kenri_men, '']
        arr_kakuchi.push [fugo, oneR.gokeiRec.block_num, oneR.gokeiRec.lot_num]
        kenri_men_sum += oneR.gokeiRec.kenri_men.to_f
        kanchi_men_sum += oneR.gokeiRec.kan_men.to_f
      end
      kenri_genbuR,kanchi_genbuR = gokei_genbu_cal(kenri_men_sum, kanchi_men_sum, kijun_men)
      arr_genbu = [format("%3.2f",kenri_men_sum), kenri_genbuR, kanchi_genbuR]
      arr_oneRec.each do |oneR|
        oneR.gokeiRec.h_1n[fcode + "bun"] = arr_kakuchi
        oneR.gokeiRec.h_1n[fcode ] = arr_genbu
        oneR.gokeiRec.kenri_genbuR = kenri_genbuR
        oneR.gokeiRec.kan_genbuR = kanchi_genbuR
        gokei_seisan oneR
      end

    end
    msg
  end

  def gokei_genbu_cal kenri_s, kanchi_s, kijun_s
    return ['', ''] if kijun_s.to_f == 0
    kenri_gR = format("%3.2f\%", (1.0 - kenri_s / kijun_s) * 100 )
    kanchi_gR = format("%3.2f\%", (1.0 - kanchi_s / kijun_s) * 100 )
    [kenri_gR, kanchi_gR]
  end

  def gokei_seisan oneR
#    pp oneR
    sabun = oneR.gokeiRec.kan_hyoka_sisu.to_i - oneR.gokeiRec.kenri_sisu.to_i
    oneR.gokeiRec.choshu_sisu = oneR.gokeiRec.kofu_sisu = ' '
    return if oneR.arrZenRec[0].fudecode[/^50/]
    if sabun > 0
      oneR.gokeiRec.choshu_sisu = sabun
    else
      oneR.gokeiRec.kofu_sisu = sabun * -1
    end
  end

  def kijun_men_zan arr_OR  #基準面積（残地）を計算
    zan_num = nil
    wk_kijun_men = arr_OR[0].arrZenRec[0].kijunMen
    0.upto(arr_OR.size - 1).each do |n|
      kj = arr_OR[n].gokeiRec.kijun_men.to_f
      zan_num = n if  kj == 0
      wk_kijun_men -= kj
    end
    if zan_num
      arr_OR[zan_num].gokeiRec.kijun_men = format("%3.2f", wk_kijun_men)
    end
    kenri_sisu_bunkatu arr_OR
    if zan_num or wk_kijun_men == 0
      return nil
    else
      return "The sum of kijun_men error! Diff = #{wk_kijun_men}"
    end
  end

  def kenri_sisu_bunkatu arr_OR
    #権利指数の計算＆丸め誤差を修正
    diff = arr_OR[0].arrZenRec[0].kenriSisu.to_i
    zen_m2 = arr_OR[0].arrZenRec[0].heibeiSisu.to_f
    arr_OR.each do |r|
      ks = (r.gokeiRec.kijun_men.to_f * zen_m2 * KihonCode::zosin).round
      r.gokeiRec.kenri_sisu = ks.to_s
      diff -= ks
    end
    if diff != 0
      #丸め誤差を最大面積の画地で調整
      r =arr_OR.sort{|a,b| a.gokeiRec.kijun_men <=> b.gokeiRec.kijun_men}.last
      x = r.gokeiRec.kenri_sisu
      r.gokeiRec.kenri_sisu = (x.to_i + diff).to_s
    end
    arr_OR.each do |r|
      kenri_men_cal r
    end
  end

  def kenri_men_cal oneR
      x = oneR.gokeiRec.kenri_sisu.to_f / oneR.gokeiRec.heibei_sisu.to_f
      oneR.gokeiRec.kenri_men = format("%3.2f", round2(x) )
  end

  def conv_keisan_one oneRec, h_shusei
    oneRec.gokeiRec.hyoka_sisuA = 0
    oneRec.gokeiRec.hyoka_sisuSum = 0
    oneRec.gokeiRec.men = 0
    oneRec.h_HyokaRec.keys.sort.each do |key|
      hy = oneRec.h_HyokaRec[key]
      wk_sisu = hy.men.to_f * hy.rosenka.to_i
      hy.arr_shusei.each do |a| wk_sisu *= a[1].to_f end
      hy.sho_heibei_sisu = (wk_sisu / hy.men.to_f).round
      hy.hyoka_sisuA = (hy.sho_heibei_sisu * hy.men.to_f).round
      oneRec.gokeiRec.men += hy.men.to_f
      oneRec.gokeiRec.hyoka_sisuA += hy.hyoka_sisuA
      oneRec.gokeiRec.hyoka_sisuSum += hy.hyoka_sisuA
      hy.arr_soku.each do |rec|
        oneRec.gokeiRec.hyoka_sisuSum += rec.hyoka_sisuB.to_i
      end
    end
    oneRec.gokeiRec.men = format("%3.2f", oneRec.gokeiRec.men)
    oneRec.gokeiRec.heibei_sisu = (oneRec.gokeiRec.hyoka_sisuSum / oneRec.gokeiRec.men.to_f).round
    oneRec.gokeiRec.kan_hyoka_sisu =
        (oneRec.gokeiRec.heibei_sisu * oneRec.gokeiRec.kan_men.to_f).round
#pp oneRec
  end

  def floor2 x
    x.to_s.to_d.floor(2).to_f
  end

  def round2 x
    x.to_s.to_d.round(2).to_f
  end



end #end of module
