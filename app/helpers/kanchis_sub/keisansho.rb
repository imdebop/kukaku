
#2004/04/28
#	usage : C:>ruby keisansho.rb 01000000 12999999 [p/nil [s]]

module Keisansho
#$KCODE = 's'
require 'kanchis_sub/keisansho_kyotu'
require_dependency 'kanchis_sub/exhandle'
#require 'kanchi_read'
require 'pp'				#2016/03/19
require 'yaml'				#2016/03/21

#@startFude = ARGV[0]
#@endFude = ARGV[1]
#@printFlg = ARGV[2]
#@stopFlg = ARGV[3]
#2016/3/21  #test data  >>>>>>
@startFude = "01000000"
@endFude = "99999999"
@printFlg =nil
@stopFlg = nil

#test_rec = YAML.load(File.read('kanchis_sub/yaml_test.txt'))

#2016/3/21    test data       <<<<<<<<


###########  レコード編集  ########################
class HyokaDtlKeisan < HyokaDtlRange
	def initialize
		super
	end
end

class KanchiKeisansho < ZenKeisanRange
	attr_reader :gokei_men, :gokei_sisu_A, :gokei_sisu_B, :gokei_hyoka_sisu,
				:gokei_heibei_sisu
	def initialize
		hyokaDtl_init
	end
end

def get_zenGokeiRec( arr_zen )
	r = ZenGokeiRec.new
	r.fudesu = arr_zen.size.to_s
#	r.hyoka_sisuSum =
	r.toki_menSum = 0; r.kijun_menSum = 0; r.kenri_sisuSum = 0
	arr_zen.each_index{|i|
		a = arr_zen[ i ]
		r.toki_menSum += a.toki_men.to_f
		r.kijun_menSum += a.kijun_men.to_f
		r.kenri_sisuSum += a.so_sisu.to_i
	}
	return r
end


#######  main  ##################
def jikko arrRec
@startFude = "01000000"
@endFude = "99999999"
@printFlg =nil
@stopFlg = nil

#arrRec = [ YAML.load(File.read('yaml_test.txt')) ]
#	a = ExRead.new
#	arrRec, h_1n = a.ex_get
#pp arrRec

	arrTempData = []

#	rec2temp( arrTempData, arrRec, h_1n )
  h_1n = {}   #dummy
	rec2temp( arrTempData, arrRec, h_1n )
#exit
#	wksheet = keisansho_init
  wksheet = ExHandle.new				#2016/3/21

	keisanshoPrint( arrTempData, wksheet, h_1n )		#2016/3/17
#exit
#	@ex.excelClose
end

def rec2temp( arrTempData, arrRec, h_1n )
#p arrRec ##### test code ########
	arrRec.each{ | uRec |
		tempRecs = TempRecs.new
		tempRecs.sort_key = uRec.sort_key
		if tempRecs.sort_key =~ /^(........)([a-z])$/
			tempRecs.kanchi_type = '1n'
#		bun_other = nil
#			bun_other = []
#			a_wk = h_1n[ $1 + "memo" ]
#			a_wk.each do |a|
#				bun_other.push(a) if a[0] == $2
#			end
		end
		if @startFude
			next if tempRecs.sort_key < @startFude
			next if tempRecs.sort_key > @endFude
		else
			exit
		end
		pageZen( uRec, tempRecs )
		pageHyo( uRec, tempRecs )
		pageHeader( uRec, tempRecs )
		pageGokei( uRec, tempRecs )
		arrTempData.push( tempRecs )
	}
end

def pageHeader( uRec, tempRecs )
	zr = uRec.arrZenRec[0]
	gk = uRec.gokeiRec
	tempRecs.header.gaikuNum = gk.block_num
	tempRecs.header.lotNum = gk.lot_num
	tempRecs.header.sho_code = zr.sho_code
	tempRecs.header.shimei = zr.shimei
end

def pageGokei( uRec, tempRecs )
	tempRecs.hyokaGokei = uRec.gokeiRec
end

def pageZen( uRec, tempRecs )
	### 整理前合計
	tempRecs.zenGokei.fudesu = uRec.arrZenRec.size
	if tempRecs.kanchi_type != '1n'
	  uRec.arrZenRec.each{|z|
		tempRecs.zenGokei.toki_menSum += z.tokiMen.to_f
		tempRecs.zenGokei.kijun_menSum += z.kijunMen.to_f
		tempRecs.zenGokei.hyoka_sisuSum += z.hyokaSisu.to_i
		tempRecs.zenGokei.kenri_sisuSum += z.kenriSisu.to_i
	  }
	else
		tempRecs.zenGokei.toki_menSum = ''
		tempRecs.zenGokei.kijun_menSum = uRec.gokeiRec.kijun_men
		tempRecs.zenGokei.hyoka_sisuSum = uRec.gokeiRec.kenri_sisu
	end
	### 整理前ページごとの編集
	i = 0
	zen = uRec.arrZenRec
	while true
		if zen.size < i + 7
			tempRecs.ar2ZenRec.push( zen[ i, 99 ] )
			break
		else
			tempRecs.ar2ZenRec.push( zen[ i, 7 ] )
			i += 7
			break if zen[ i ].nil?
		end
	end
end

def pageHyo( uRec, tempRecs )
	page = 0
	tempRecs.ar2HyokaRec[ 0 ] = []
	hyokaRows = 6
	rowsSum = 0
	uRec.h_HyokaRec.keys.sort.each{| key |
		#hr is HyokaRec
		hr = uRec.h_HyokaRec[ key ].dup
		hr.rows = 1	#レコード毎の行数
		#修正項目の行数決定
		if hr.arr_shusei.size > 5
			hr.rows = 2
			ar2shusei = [ hr.arr_shusei[0, 5], hr.arr_shusei[5, 99] ]
		else
			ar2shusei = [ hr.arr_shusei ]
		end
		hr.arr_shusei = ar2shusei
		n = hr.arr_soku.size
		hr.rows = n if n > hr.rows
		#１ページの行数を超えるかの判定
		if rowsSum + hr.rows <= hyokaRows
			rowsSum += hr.rows
			tempRecs.ar2HyokaRec[ page ].push( hr )
		else
			page += 1
			tempRecs.ar2HyokaRec[ page ] = [ hr ]
			rowsSum = hr.rows
		end
	}
end

#2016/3/21  method delete
#def keisansho_init
#	@ex = ExcelGet.new
#	wkbook=@ex.wkbookGet( Dir.pwd + "/換地計算書")
#	page = 1
#	wksheet = wkbook.Worksheets(page)
#	wksheet.Activate
#
#	return wksheet
#end



def keisanshoPrint( arrTempData, wksheet, h_1n )		#2016/3/17
	arrOutData = []
	pRec = PageRec.new
	arrTempData.sort{|a,b|
		if a.sort_key < b.sort_key
			1
		else
			-1
		end
	}.each{ | tempRecs |
		tempRecs.sort_key =~ /^(........)([a-z])$/

		bunkatu = $2								#2016/3/17
		while zen = tempRecs.ar2ZenRec.shift
			pRec.header = tempRecs.header
			pRec.arrZenRec = zen
			break if tempRecs.ar2ZenRec.size == 0
			pRec.zenGokei = nil
			pRec.arrHyokaRec = nil
			pRec.hyokaGokei = nil
			arrOutData.push( pRec )
			pRec = PageRec.new
			pRec.header = tempRecs.header
		end
		pRec.zenGokei = tempRecs.zenGokei
		hyo = tempRecs.ar2HyokaRec.shift
		pRec.arrHyokaRec = hyo
		while true
#			hyo = tempRecs.ar2HyokaRec.shift
			if tempRecs.ar2HyokaRec.size == 0
				pRec.hyokaGokei = tempRecs.hyokaGokei
				#2016/3/23 add 1 line
#				pRec.xl_fname = tempRecs.hyokaGokei.block_num + "BL" + tempRecs.hyokaGokei.lot_num
				pRec.xl_fname = xl_fname( tempRecs.hyokaGokei.block_num, tempRecs.hyokaGokei.lot_num ) #2016/3/26
				arrOutData.push( pRec )
				pRec = PageRec.new
				break
			end
			arrOutData.push( pRec )
#			pRec.arrZenRec = nil
#			pRec.zenGokei = nil
#			pRec.hyokaGokei = nil
			pRec = PageRec.new
			pRec.arrHyokaRec = tempRecs.ar2HyokaRec.shift
			pRec.header = tempRecs.header
		end
#p arrOutData[1].arrZenRec;exit
		pageOut( arrOutData, wksheet, bunkatu )
		arrOutData = []
		pRec = PageRec.new
	}
end

def xl_fname bl, lot					#2016/3/26
	if lot[/^(\d+)(-\d+)$/]
		wklot = $1; eda = $2
	else
		wklot = lot; eda = nil
	end
  str = sprintf( "%02dBL%02d", bl.to_i, wklot.to_i)
  str = str + eda if eda
  str
end

def pageOut( arrOutData, wksheet, bunkatu )
	zk = ZenKeisanRange.new
	zg = ZenGokeiRange.new
	hy = HyokaDtlRange.new
	gk = KanGokeiRange.new
	hdOut( nil, wksheet, zk )
	zenOut( nil, wksheet, zk )
	zenOutGokei( nil, wksheet, zg, bunkatu )
	hyokaOut( nil, wksheet, hy )
	if arrOutData.size > 1
		cnt = arrOutData.size 
	else
		cnt = 0
	end
	arrOutData.reverse.each{ | pRec |
		hdOut( pRec.header, wksheet, zk )
		zenOut( pRec.arrZenRec, wksheet, zk )
		zenOutGokei( pRec, wksheet, zg, bunkatu )
		hyokaOut( pRec.arrHyokaRec, wksheet, hy )
		hyokaOutGokei( pRec.hyokaGokei, wksheet, gk, bunkatu )
#2016/3/21 >>> #16/6/29
		if cnt > 0
			@fname = pRec.xl_fname unless pRec.xl_fname.nil?
			wksheet.write (@fname + "その" + cnt.to_s)
			cnt -= 1
		else
			wksheet.write pRec.xl_fname
		end
#		wksheet.close
#		wksheet.PrintOut({'Copies'=>1}) if @printFlg == 'p'
#STDIN.gets if @stopFlg
# <<<<<<
	}
end

def hdOut( hd, wksheet, zk )
	wksheet.Range(zk.clearRangeHd).ClearContents
	return if hd.nil?
	wksheet.Range(zk.blockRange).value = [ hd.gaikuNum ]
	wksheet.Range(zk.lotRange).value = [ hd.lotNum ]
	wksheet.Range(zk.sho_codeRange).value = [ hd.sho_code ]
	wksheet.Range(zk.shimeiRange).value = [ hd.shimei ]
end

def zenOut( arrZen, wksheet, zk )
	wksheet.Range( zk.clearRange ).ClearContents
	return if arrZen.nil?
	arrZen.each_with_index{| a,i |
		zk.setRow( i )
		wksheet.Range(zk.chomeiRange).value = [ a.chomei ]
		wksheet.Range(zk.chibanRange).value = [ a.chiban ]
		wksheet.Range(zk.tokiChimokuRange).value = [ a.tokiChimoku ]
		wksheet.Range(zk.tokiChisekiRange).value = [ TokiMen.edit(a.tokiChimoku, a.tokiMen) ]
		wksheet.Range(zk.kijunChisekiRange).value = [ a.kijunMen ]
		wksheet.Range(zk.heibeiSisuRange).value = [ a.heibeiSisu ]
		wksheet.Range(zk.hyokaSisuRange).value = [ a.hyokaSisu ]
		wksheet.Range(zk.kenriSisuRange).value = [ a.kenriSisu ]
	}
end

def zenOutGokei( pRec, wksheet, zg, bunkatu )
	wksheet.Range( zg.clearRange1).ClearContents
	wksheet.Range( zg.clearRange2).ClearContents
	wksheet.Range( zg.clearRange3).ClearContents
	return if pRec.nil?
	return if pRec.zenGokei.nil?
	g = pRec.zenGokei
	wksheet.Range( zg.fudesu ).value = [ g.fudesu ] if g.toki_menSum.to_f > 0
	wksheet.Range( zg.toki_menSum ).value = [ g.toki_menSum ]
	wksheet.Range( zg.kijun_menSum ).value = [ g.kijun_menSum ]
	if bunkatu
		str = "分割符号:  " + bunkatu
		wksheet.Range( zg.bikoSum ).value = [ str ]
	else
		wksheet.Range( zg.hyoka_sisuSum ).value = [ g.hyoka_sisuSum ]
                #2017/4/4 add 1 line
		wksheet.Range( zg.kenri_sisuSum ).value = [ g.kenri_sisuSum ]
	end
	wksheet.Range( zg.kenri_sisuSum ).value = [ pRec.hyokaGokei.kenri_sisu ] if pRec.hyokaGokei
end

def hyokaOut( arrHyoka, wksheet, hy )
	wksheet.Range(hy.clearRangeHyo).ClearContents
	return if arrHyoka.nil?
	currRow = 0
	arrHyoka.each_index{|i|
		hy.set_sho( currRow  )
		a = arrHyoka[ i ]
		wksheet.Range(hy.fugo).value = [ a.fugo ]
		wksheet.Range(hy.men).value = [ a.men ]
		wksheet.Range(hy.rosen).value = [ a.rosen ]
		wksheet.Range(hy.rosenka).value = [ a.rosenka ]
		a.arr_shusei.each_with_index{ | arr, j |
			hy.set_sho( currRow + j )
			arr.each_with_index{| val, k |
				item, rate = val
				wksheet.Range( hy.arr_sho_shuseiItem[ k ] ).value = item
				wksheet.Range( hy.arr_sho_shuseiRate[ k ] ).value = rate
			}
		}
		wksheet.Range(hy.sho_heibei_sisu).value = [ a.sho_heibei_sisu ]
		wksheet.Range(hy.hyoka_sisuA).value = [ a.hyoka_sisuA ]

		wkRow = currRow
		a.arr_soku.each_with_index{| sk, i |
			hy.set_sok_hai( wkRow  + i)
			wksheet.Range(hy.keitai).value = [ sk.keitai ]
			wksheet.Range(hy.sokRosen).value = [ sk.rosen ]
			wksheet.Range(hy.sokRosenka).value = [ sk.rosenka ]
			wksheet.Range(hy.kasanRate).value = [ sk.kasanR ]
			wksheet.Range(hy.kasanMaguchi).value = [ sk.len ]
			sk.arr_shusei.each_with_index{| val, i |
				item, rate = val
				wksheet.Range( hy.arr_sok_shuseiItem[ i ] ).value = item
				wksheet.Range( hy.arr_sok_shuseiRate[ i ] ).value = rate
			}
			wksheet.Range(hy.hyoka_sisuB).value = [ sk.hyoka_sisuB ]
		}
		currRow += a.rows
	}
end

def hyokaOutGokei( gokei, wksheet, gk, bunkatu )
#pp gokei
	wksheet.Range( gk.clearRange1).ClearContents
	wksheet.Range( gk.clearRange2).ClearContents
	wksheet.Range( gk.clearRangeMemo).ClearContents		#2016/3/16
	return if gokei.nil?
	#2016/3/27  Not print "0"
	wksheet.Range(gk.menSum).value = [ men2keta(gokei.men) ] if gokei.men.to_f > 0
	wksheet.Range(gk.hyoka_sisuA).value = [ gokei.hyoka_sisuA ] if gokei.hyoka_sisuA.to_f > 0
	wksheet.Range(gk.hyoka_sisuB).value = [ gokei.hyoka_sisuB ] if gokei.hyoka_sisuB.to_f > 0
	wksheet.Range(gk.hyoka_sisuSum).value = [ gokei.hyoka_sisuSum ] if gokei.hyoka_sisuSum.to_f > 0
	wksheet.Range(gk.heibei_sisu).value = [ gokei.heibei_sisu ] if gokei.heibei_sisu.to_f > 0

	wksheet.Range(gk.kenri_men).value = [ men2keta(gokei.kenri_men) ] if gokei.kenri_men.to_f > 0
	wksheet.Range(gk.kenri_genbuR).value = [ gokei.kenri_genbuR ]

	wksheet.Range(gk.block_num).value = [ gokei.block_num ]
	wksheet.Range(gk.lot_num).value = [ gokei.lot_num ]
	wksheet.Range(gk.kan_men).value = [ men2keta(gokei.kan_men) ]
	wksheet.Range(gk.kan_hyoka_sisu).value = [ gokei.kan_hyoka_sisu]
	wksheet.Range(gk.kan_genbuR).value = [ gokei.kan_genbuR ] if gokei.kijun_men.to_f > 0
	wksheet.Range(gk.choshu_sisu).value = [ gokei.choshu_sisu ] if gokei.choshu_sisu.to_f > 0
	wksheet.Range(gk.kofu_sisu).value = [ gokei.kofu_sisu ] if gokei.kofu_sisu.to_f > 0

	if bunkatu																				#2016/3/26
    arr_bun = nil
		gokei.h_1n.each do |key, val|
			if key[/bun$/]
				arr_bun = val
				break
			end
	  end
		wksheet.Range(gk.memoRange 0).value =	 ["分割","Block","Lot"]
    row = 1
		1.upto(arr_bun.size) do |n|
			next if arr_bun[ n - 1][ 0 ] == bunkatu
			wksheet.Range(gk.memoRange row).value = arr_bun[ n - 1 ]
			row += 1
		end
		row += 2 
	else
		row = 1
	end

	unless gokei.arrMemo.nil?
		gokei.arrMemo.each do |s|
			wksheet.Range(gk.memoRange1 row).value =	 [ s ]
			row += 1
		end
	end
	gokei.kento = "" if gokei.kento.nil?
	wksheet.Range(gk.kento).value = [ gokei.kento ]

end

#20160419 add
def men2keta men
	format("%3.2f", men.to_f)
end

end  # of module

#jikko [ test_rec ]

