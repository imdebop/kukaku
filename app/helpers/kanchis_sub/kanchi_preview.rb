module KanchiPreview

def zen_preview f_code
  obj = land = Land.find_by( f_code: f_code)
  return [nil, ""] if obj.nil?
#  shozai = KihonCode.fcode_to_shozai f_code
  hdr = "  所  在             基準地積   評定指数  権利指数  所有者\n"
#  str = hdr + format("%-16s%3.2f   % 8d     % 8d     %s",
#                shozai, obj.kijun_men, obj.hyotei_sisu, obj.sho_code )
  str = hdr + edit_zen_line( obj )
  if @arr_oneRec
    arr_obj = @arr_oneRec[0].arrZenRec[1..-1].map do |zen|
      Land.find_by(f_code: zen.fudecode)
    end
  else
    arr_obj = land.children
  end
  if arr_obj.size > 0
    arr_obj.each do |obj|
      str += edit_zen_line(obj)
    end
  end
  [land, str]
end

def edit_zen_line obj
  shozai = KihonCode.fcode_to_shozai obj.f_code
  str = format("%-16s%6.2f   % 8d  % 8d    %s\n",
                shozai, obj.kijun_men, obj.hyotei_sisu, obj.kenri_sisu, obj.sho_code )
  str
end

def kan_preview land
  str_all = ""
  if @arr_oneRec
    arr_obj = @arr_oneRec
  elsif land.horyu
    a = Kanchi.find_by(f_code: land.horyu)
    arr_obj = [ YAML.load(a.tmp_text) ]
  else
    arr_obj = land.kanchis.map do |a| YAML.load(a.tmp_text) end
  end
  arr_obj.sort{|a,b| a.sort_key <=> b.sort_key }.each do |obj|
    str = "\n#{kan_kijun(obj)}"
    str += "街区-画地---㎡指数---権利面積---減歩率----換地---減歩率----徴・交----過不足\n"
    gk = obj.gokeiRec
    kabusoku = gk.kan_men.to_f - gk.kenri_men.to_f
    str += format(" % 2s %4s    % 4d     % 7.2f   % 6.2f  % 7.2f  % 5.2f  %6s    %5.2f\n",
          gk.block_num, gk.lot_num, gk.heibei_sisu.to_i,
          gk.kenri_men.to_f, gk.kenri_genbuR.to_f, gk.kan_men.to_f,
          gk.kan_genbuR.to_f, seisan(gk), kabusoku )

    @bl_lt = gk.block_num + "BL" + gk.lot_num  #20160709
    str_all += str + hyoka_preview( obj )
  end
  str_all
end

def kan_kijun obj
  if obj.sort_key[/([a-z])$/]
    str = format("分割: %s  基準面積:%7.2f  権利指数 %8d\n",
        $1, obj.gokeiRec.kijun_men.to_f, obj.gokeiRec.kenri_sisu.to_i)
    return str
  else
    return ""
  end
end

def hyoka_preview obj
  indent = " " * 5
  hdr = indent + "符号---面積--正面路線--路線価-----------修  正----------\n"
  rows = ""
  str = hdr
  obj.h_HyokaRec.keys.sort.each do |key|
    hy = obj.h_HyokaRec[key]
    fugo = hy.fugo
    fugo = "-" if fugo == ""
    str += indent + format(" %s    %4.2f  %8s   %4s  %s\n",
      fugo, hy.men, hy.rosen, hy.rosenka, edit_shusei(hy.arr_shusei) )
    str += hy_sokuhai hy

  end
  str
end

def hy_sokuhai hy
  indent = " " * 10
  arr = hy.arr_soku
  return "" if arr.size == 0
  hdr = indent + "形態--路線番号--路線価--加算率--間口-----修  正------\n"
  wk = hdr
  arr.each do |a|
    wk += indent + format(" %s   % -8s % 4s   % 5s %4s  %s\n",
      a.keitai, a.rosen, a.rosenka,
      a.kasanR.strip, a.len, edit_shusei(a.arr_shusei) )
  end
  wk
end

def edit_shusei arr
  wk = ""
  arr.each do |a|
    wk += a.join(":").gsub(" ","") + " "
  end
  wk
end

def seisan gk
  kofu = gk.kofu_sisu.to_i
  choshu = gk.choshu_sisu.to_i
  if kofu == 0 and choshu == 0
    return "   0"
  else
    if kofu > 0
      return format("交 %d", kofu)
    else
      return format("徴 %d", choshu)
    end
  end
end

end #end of module
