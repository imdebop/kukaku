#20040302
#20040917
#
class ExcelGet
  def wkbookGet(fname)
	require 'win32ole'
	if	@application = WIN32OLE.new('Excel.Application')
			#no operation
		else
			@application = WIN32OLE.connect('Excel.Application')
		end
		
	@application.DisplayAlerts = FALSE
	@application.visible = TRUE
	wkbook = @application.Workbooks.open(fname)
	return wkbook
  end

  def excelClose
	@application.Quit
  end
end


class KeisanKyotu
	def setRow( n )
		@rowUpper = @hyokaStartRow + n * 2
		@rowLower = @rowUpper + 1
	end
	def rangeSet(col,row)
		return col + row.to_s + ":" + col + row.to_s
	end
end

class ZenGokeiRange < KeisanKyotu
	attr_reader :fudesu, :toki_menSum, :kijun_menSum, :hyoka_sisuSum,
				:kenri_sisuSum, :bikoSum,
				:clearRange1, :clearRange2, :clearRange3
	def initialize
		@fudesu = rangeSet( "d", 23 )
		@toki_menSum = rangeSet( "j", 23 )
		@kijun_menSum = rangeSet( "j", 24 )
		@hyoka_sisuSum = rangeSet( "o", 23 )
		@kenri_sisuSum = rangeSet( "o", 24 )
		@bikoSum = rangeSet( "r", 23 )
		@clearRange1 = "d23:f24"
		@clearRange2 = "j23:q24"
		@clearRange3 = "r23:u24"
	end
end

class ZenKeisanRange < KeisanKyotu
	attr_reader :sho_codeRange, :shimeiRange, :clearRange, :clearRangeHd,
							:blockRange, :lotRange
	def initialize
		@juzenStartRow = 9
		@juzenRows = 7
		@blockRange = "v4:v4"
		@lotRange = "y4:y4"
		@sho_codeRange = "aa4:aa4"
		@shimeiRange = "ad4:ad4"
		@clearRange = "a9:u22"
		@clearRangeHd = "aa4:ak5"
	end
	def setRow(n)
		@rowUpper = @juzenStartRow + n * 2
		@rowLower = @juzenStartRow + 1 + n * 2
	end
	def chomeiRange
		return rangeSet("a", @rowUpper)
	end
	def chibanRange
		return rangeSet("d", @rowUpper)
	end
	def tokiChimokuRange
		return rangeSet("g", @rowUpper)
	end
	def genkyoChimokuRange
		return rangeSet("g", @rowLower)
	end
	def tokiChisekiRange
		return rangeSet("j", @rowUpper)
	end
	def kijunChisekiRange
		return rangeSet("j", @rowLower)
	end
	def heibeiSisuRange
		return rangeSet("m", @rowUpper)
	end
	def hyokaSisuRange
		return rangeSet("o", @rowUpper)
	end
	def kenriSisuRange
		return rangeSet("o", @rowLower)
	end
end


class HyoGokeiRange < KeisanKyotu
	attr_accessor :menSum, :hyoka_sisuA, :hyoka_sisuB, :hyoka_sisuSum, :heibei_sisu,
					:clearRange
	def initialize
		@menSum = rangeSet( "a", 44 )
		@hyoka_sisuA = rangeSet( "d", 44 )
		@hyoka_sisuB = rangeSet( "g", 44 )
		@hyoka_sisuSum = rangeSet( "j", 44 )
		@heibei_sisu = rangeSet( "m", 44 )
		@clearRange = ( "a44:n44" )
	end
end

class KanGokeiRange < HyoGokeiRange
	attr_accessor :kenri_men, :kenri_genbuR, :block_num, :lot_num,
				  :kan_men, :kan_hyoka_sisu, :kan_genbuR,
				  :choshu_sisu, :kofu_sisu, :clearRangeMemo,
				  :clearRange1, :clearRange2, :kento
	def initialize
		super
		@kenri_men = rangeSet( "o", 44 )
		@kenri_genbuR = rangeSet( "r", 44 )
		@block_num = rangeSet( "t", 44 )
		@lot_num = rangeSet( "v", 44 )
		@kan_men = rangeSet( "x", 44 )
		@kan_hyoka_sisu = rangeSet("aa", 44 )
		@kan_genbuR = rangeSet( "ad", 44 )
		@choshu_sisu = rangeSet( "af", 44 )
		@kofu_sisu = rangeSet( "ai", 44 )
		@kento = rangeSet( "ad", 6 )
		@clearRange1 = ( "a44:f44" )
		@clearRange2 = ( "j44:ak44" )
		@memoFirstRow = 9									#2016/3/19
		@clearRangeMemo = ( "w#{@memoFirstRow}:y19" )		#2016/3/19
	end

	def memoRange n			########## 2016/3/19  ���\�b�h�ǉ�
		row = @memoFirstRow + n
		return "w#{row}:y#{row}"
	end

	def memoRange1 n			########## 2016/7/8
		row = @memoFirstRow + n
		return "w#{row}:w#{row}"
	end
end

#class KanchiGokeiRange < HyoGokeiRange
#	#���n�v�Z���p
#	attr_accessor :kenri_men, :genbuR, :gaiku_num, :lot_num, :kanchi_men,
#				:hyoka_sisu, :kanchiR, :choshu_sisu, :kofu_sisu
#end

class GaikuKeisanRange < KeisanKyotu
	attr_reader :gaiku, :gaikuMen,
				:kakuchi, :men, :rosen, :rosenka,
				:arr_sho_shuseiItem, :arr_sho_shuseiRate,
				:hyoka_sisuA,
				:keitai, :shurui, :sokRosen, :sokRosenka,
				:kasanRate, :maguchi, :okuyuki,
				:arr_sok_shuseiItem, :arr_sok_shuseiRate,
				:hyoka_sisuB, :gokei_sisu, :heibei_sisu, :hyoka_sisu,
				:clearRangeDtl, :clearRangeSum,
				:rows,
				:menTTL, :sisuTTL
	def initialize
		@hyokaStartRow = 10
		@rows = 16
		@gaiku = "ag4:ag4"
		@gaikuMen = "aj4:aj4"
		@menTTL = "c42:c42"
		@sisuTTL = "ak42:ah42"
		@arr_sho_shuseiItem = []
		@arr_sho_shuseiRate = []
		@arr_sok_shuseiItem = []
		@arr_sok_shuseiRate = []
		@clearRangeDtl ="a10:am41"
		@clearRangeSum ="c42:am43"
	end
	def setDtl( row )
		setRow( row  )
		@kakuchi = rangeSet( "a", @rowUpper )
		@men = rangeSet( "c" , @rowUpper )
		@rosen = rangeSet( "f" , @rowUpper )
		@rosenka = rangeSet( "f", @rowLower )
		set_arr_sho_shusei( row )
		@hyoka_sisuA = rangeSet( "r", @rowUpper )
		@keitai =rangeSet( "u", @rowUpper )
		@shurui =rangeSet( "u", @rowLower )
		@sokRosen = rangeSet( "v" , @rowUpper )
		@sokRosenka = rangeSet( "v", @rowLower )
		@kasanRate = rangeSet( "x" , @rowUpper )
		@maguchi = rangeSet( "x" , @rowLower )
		@okuyuki = rangeSet( "y" , @rowLower )
		set_arr_sok_shusei( row )
		@hyoka_sisuB = rangeSet( "ad", @rowUpper )
		@gokei_sisu = rangeSet( "ag", @rowUpper )
		@heibei_sisu = rangeSet( "ai", @rowUpper )
		@hyoka_sisu = rangeSet( "ak", @rowUpper )
	end
	def set_arr_sho_shusei( row )
		col = "h"
		( 0 ).upto( 4 ){|n|
			@arr_sho_shuseiItem[ n ] = rangeSet( col, @rowUpper )
			@arr_sho_shuseiRate[ n ] = rangeSet( col, @rowLower )
			col.succ!.succ!
		}
	end
	def set_arr_sok_shusei( row )
		col = "z"
		( 0 ).upto( 1 ){|n|
			@arr_sok_shuseiItem[ n ] = rangeSet( col, @rowUpper )
			@arr_sok_shuseiRate[ n ] = rangeSet( col, @rowLower )
			col.succ!.succ!
		}
	end
end

class HyokaDtlRange < KeisanKyotu
	attr_reader :fugo, :men, :rosen, :rosenka,
			 :arr_sho_shuseiItem, :arr_sho_shuseiRate,
			:sho_heibei_sisu, :hyoka_sisuA,
			:keitai, :sokRosen, :sokRosenka,
			:kasanRate, :kasanMaguchi,
			:arr_sok_shuseiItem, :arr_sok_shuseiRate,
			:hyoka_sisuB,
			:clearRangeHyo

	def initialize
		@hyokaStartRow = 30
		@hyokaRows = 6
		@arr_sho_shuseiItem = []
		@arr_sho_shuseiRate = []
		@arr_sok_shuseiItem = []
		@arr_sok_shuseiRate = []
		@clearRangeHyo ="a30:ah41"
	end
	def set_sho( row )
		setRow( row )
		@fugo = rangeSet( "a", @rowUpper )
		@men = rangeSet( "c" , @rowUpper )
		@rosen = rangeSet( "f" , @rowUpper )
		@rosenka = rangeSet( "f", @rowLower )
		set_arr_sho_shusei( row )
		@sho_heibei_sisu = rangeSet( "r", @rowUpper )
		@hyoka_sisuA = rangeSet( "t", @rowUpper )
	end
	def set_arr_sho_shusei( row )
		col = "h"
		( 0 ).upto( 4 ){|n|
			@arr_sho_shuseiItem[ n ] = rangeSet( col, @rowUpper )
			@arr_sho_shuseiRate[ n ] = rangeSet( col, @rowLower )
			col.succ!.succ!
		}
	end
	def set_sok_hai( row )
		setRow( row )
		@keitai = rangeSet( "w", @rowUpper )
		@sokRosen = rangeSet( "x" , @rowUpper )
		@sokRosenka = rangeSet( "x", @rowLower )
		@kasanRate = rangeSet( "z" , @rowUpper )
		@kasanMaguchi = rangeSet( "z" , @rowLower )
		set_arr_sok_shusei( row )
		@hyoka_sisuB = rangeSet( "af", @rowUpper )
	end
	def set_arr_sok_shusei( row )
		col = "ab"
		( 0 ).upto( 4 ){|n|
			@arr_sok_shuseiItem[ n ] = rangeSet( col, @rowUpper )
			@arr_sok_shuseiRate[ n ] = rangeSet( col, @rowLower )
			col.succ!.succ!
		}
	end
end

###########  ���R�[�h  ########################
class OneUnitRec
	attr_accessor :arrZenRec, :h_HyokaRec, :gokeiRec, :sort_key
	def initialize
		@arrZenRec = []
		@h_HyokaRec = {}
	end
end

class PageHeader
	#�O�]���A���n�v�Z���@����
	attr_accessor :gaikuNum, :lotNum, :sho_code, :shimei
end

class TempRecs
	attr_accessor :header, :ar2ZenRec, :zenGokei, :ar2HyokaRec, :hyokaGokei,
					:sort_key, :kanchi_type
	def initialize
		@sort_key = ''
		@kanchi_type = nil
		@header = PageHeader.new
		@zenGokei = ZenGokeiRec.new
		@hyokaGokei = HyoGokeiRec.new
		@ar2ZenRec = []
		@ar2HyokaRec = []
	end
end

class PageRec
	attr_accessor :sort_key, :header, :arrZenRec, :zenGokei, :arrHyokaRec,
				 :hyokaGokei
end

class ZenRec
	attr_accessor :chomei, :chiban, :tokiChimoku, :genkyoChimoku,
				:tokiMen, :kijunMen, :heibeiSisu, :hyokaSisu, :kenriSisu,
				:fudecode, :sho_code, :shimei
end

class ZenRecKan < ZenRec
	attr_accessor :so_sisu
end


class ZenGokeiRec
	attr_accessor :fudesu, :toki_menSum, :kijun_menSum, :hyoka_sisuSum,
					:kenri_sisuSum
	def initialize
		@toki_menSum = 0
		@kijun_menSum = 0
		@hyoka_sisuSum = 0
		@kenri_sisuSum = 0
	end
end

class HyoGokeiRec
	attr_accessor :men, :hyoka_sisuA, :hyoka_sisuB, :hyoka_sisuSum,
				 :heibei_sisu
end

class KanGokeiRec < HyoGokeiRec
	attr_accessor :toki_men, :kijun_men, :kenri_sisu,
				  :kenri_men, :kenri_genbuR, :block_num, :lot_num,
				  :kan_men, :kan_hyoka_sisu, :kan_genbuR,
				  :choshu_sisu, :kofu_sisu, :arr_other_bun,	#2016/03/19
				  :arrMemo, :kento #20160713
end

class HyokaRec
	attr_accessor :rows, :fugo, :men, :rosen, :rosenka, :arr_shusei,
					:sho_heibei_sisu, :hyoka_sisuA, :arr_soku, :wk_heibei_sisu
	def initialize
		@arr_shusei = []
		@arr_soku = []
	end
end

class SokuHaiRec
	attr_accessor :keitai, :rosen, :rosenka, :len, :kasanR,
			 :arr_shusei, :hyoka_sisuB
	def initialize
		@arr_shusei = []
	end
end

class GaikuDtlRec
	attr_reader :kakuchi, :men, :rosen, :rosenka,
				:arr_sho_shuseiItem, :arr_sho_shuseiRate,
				:hyoka_sisuA,
				:keitai, :sokRosen, :sokRosenka, :kasanRate,
				:arr_sok_shuseiItem, :arr_sok_shuseiRate,
				:hyoka_sisuB, :sisu_gokei, :heibei_sisu, :hyoka_sisu
	def initialize
		@arr_sho_shuseiItem = []
		@arr_sho_shuseiRate = []
		@arr_sok_shuseiItem = []
		@arr_sok_shuseiRate = []
	end
end

########### ���̑� ###############
class HokoConv
	attr_reader :num2news
	def initialize
		@num2news = { "1"=>[ 1.030, "S" ], "2"=>[ 1.000, "E" ], "3"=>[ 1.000,"W" ],
					"4"=>[ 0.970, "N" ], "5"=>[ 1.015,"SE" ], "6"=>[ 1.015, "SW" ],
					"7"=>[ 0.985, "NE" ], "8"=>[ 0.985, "NW" ] }
	end
end

#20160419	add
class TokiMen
	def self.edit chimoku, men
		return '' if men.to_f == 0
		if chimoku[/^宅地/] or men.to_f < 10
			format("%3.2f", men.to_f)		
		else
			format("%d  ", men.to_i)
		end
	end
end