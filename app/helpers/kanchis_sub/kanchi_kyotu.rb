###########  レコード  ########################
#2016/4/2 Add module
module KanchiKyotu
	def kCode_to_kakuchiNo kanchi_code
		kanchi_code[/^(...)(...)(..)/]
	  return kanchi_code if $1.nil? or $2.nil?
		str = sprintf("%3dBL% 3dL ", $1.to_i, $2.to_i)
		str += "-" + format("% 2d",$3.to_i) if $3.to_i > 0
		str
	end

	#2016/5/17
	def horyu_code blk, lot_full
		lot, eda = lot_full.split(/-/)
		if eda.nil?
			format("50%03d%03d", blk, lot)
		else
			format("50%03d%03d%02d", blk, lot, eda)
		end
	end

end #end of module


#2016/3/21 Add h_1n
class OneUnitRec
	attr_accessor :arrZenRec, :h_HyokaRec, :gokeiRec, :sort_key
	def initialize
		@arrZenRec = []
		@h_HyokaRec = {}
	end
end

class PageHeader
	#前評価、換地計算書　共通
	attr_accessor :gaikuNum, :lotNum, :sho_code, :shimei
end

class TempRecs
	attr_accessor :header, :ar2ZenRec, :zenGokei, :ar2HyokaRec, :hyokaGokei,
					:sort_key, :kanchi_type
	def initialize
		@sort_key = ''
		@kanchi_type = nil
		@header = PageHeader.new
		@zenGokei = ZenGokeiRec.new
		@hyokaGokei = HyoGokeiRec.new
		@ar2ZenRec = []
		@ar2HyokaRec = []
	end
end

#2016/3/23 add xl_fname
class PageRec
	attr_accessor :sort_key, :header, :arrZenRec, :zenGokei, :arrHyokaRec,
				 :hyokaGokei, :xl_fname
end

class ZenRec
	attr_accessor :chomei, :chiban, :tokiChimoku, :genkyoChimoku,
				:tokiMen, :kijunMen, :heibeiSisu, :hyokaSisu, :kenriSisu,
				:fudecode, :sho_code, :shimei
end

class ZenRecKan < ZenRec
	attr_accessor :so_sisu
end


class ZenGokeiRec
	attr_accessor :fudesu, :toki_menSum, :kijun_menSum, :hyoka_sisuSum,
					:kenri_sisuSum
	def initialize
		@toki_menSum = 0
		@kijun_menSum = 0
		@hyoka_sisuSum = 0
		@kenri_sisuSum = 0
	end
end

class HyoGokeiRec
	attr_accessor :men, :hyoka_sisuA, :hyoka_sisuB, :hyoka_sisuSum,
				 :heibei_sisu
end

#2016/4/16 add :hyotei_sisu
class KanGokeiRec < HyoGokeiRec
	attr_accessor :toki_men, :kijun_men, :hyotei_sisu, :kenri_sisu,
				  :kenri_men, :kenri_genbuR, :block_num, :lot_num,
				  :kan_men, :kan_hyoka_sisu, :kan_genbuR,
				  :choshu_sisu, :kofu_sisu, :h_1n
end

class HyokaRec
	attr_accessor :rows, :fugo, :men, :rosen, :rosenka, :arr_shusei,
					:sho_heibei_sisu, :hyoka_sisuA, :arr_soku, :wk_heibei_sisu
	def initialize
		@arr_shusei = []
		@arr_soku = []
	end
end

class SokuHaiRec
	attr_accessor :keitai, :rosen, :rosenka, :len, :kasanR,
			 :arr_shusei, :hyoka_sisuB
	def initialize
		@arr_shusei = []
	end
end

class GaikuDtlRec
	attr_reader :kakuchi, :men, :rosen, :rosenka,
				:arr_sho_shuseiItem, :arr_sho_shuseiRate,
				:hyoka_sisuA,
				:keitai, :sokRosen, :sokRosenka, :kasanRate,
				:arr_sok_shuseiItem, :arr_sok_shuseiRate,
				:hyoka_sisuB, :sisu_gokei, :heibei_sisu, :hyoka_sisu
	def initialize
		@arr_sho_shuseiItem = []
		@arr_sho_shuseiRate = []
		@arr_sok_shuseiItem = []
		@arr_sok_shuseiRate = []
	end
end

########### その他 ###############
class HokoConv
	attr_reader :num2news
	def initialize
		@num2news = { "1"=>[ 1.030, "S" ], "2"=>[ 1.000, "E" ], "3"=>[ 1.000,"W" ],
					"4"=>[ 0.970, "N" ], "5"=>[ 1.015,"SE" ], "6"=>[ 1.015, "SW" ],
					"7"=>[ 0.985, "NE" ], "8"=>[ 0.985, "NW" ] }
	end
end