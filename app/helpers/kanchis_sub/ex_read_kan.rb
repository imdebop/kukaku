#2004/3/4

module Ex_read

##########################################
def kanchi_type_chk( x, h_1n )
#pp x
	fcode = x[0].fcode
	if x.size == 1
		type = nil
		kenri_men_sum = kenri_men_cal( x[0].zen_ken_sisu, x[0].kan_m2_sisu )
		genbuR = genbuR_cal( x[0].kijun_men, kenri_men_sum ) if kenri_men_sum.to_f > 0
    genbuR2 = genbuR_cal( x[0].kijun_men, x[0].kan_men )    #2016/3/26
		h_1n[fcode ] = [ kenri_men_sum.to_s, genbuR, genbuR2 ]
		return type
	end

	type = nil
	genbuR = ""
	kenri_men_sum = 0
  kenri_sisu_sum = 0	### 2016/2/21
  kijun_men_sum = 0	  ### 2016/2/22
  kanchi_men_sum = 0  ### 2016/3/26

	x.each{ | y |
		kenri_men = 0
		if y.chiban.to_s =~ /^.+([a-z])$/
			bun = $1
			type = '1n' 
			if bun.nil?
				print "sort_key error";exit
			end
			kenri_men = kenri_men_cal( y.zen_ken_sisu, y.kan_m2_sisu )
			kenri_men_sum += kenri_men						#2016/3/26
			genbuR = nil
			h_1n[ fcode + bun ] = [ kenri_men.to_s, genbuR ]
			h_1n_bun(h_1n, fcode, y)
		elsif y.chiban.to_s =~ /^.+/ and y.zen_ken_sisu.to_i > 0
			kenri_sisu_sum += y.zen_ken_sisu.to_i	#2016/2/21
			kijun_men_sum += y.kijun_men.to_f	    #2016/2/22
		end
		if type.nil? and y.kan_m2_sisu.to_s =~ /^.+/			#2016/3/26
			kenri_men_sum = kenri_men_cal( kenri_sisu_sum, y.kan_m2_sisu )
		end
		kanchi_men_sum += y.kan_men.to_f        #2016/3/26
#p y.zen_ken_sisu  ### test code ###
#2016/2/21		kenri_men_sum += kenri_men
	}
#p kijun_men_sum
#p kenri_men_sum
	genbuR = genbuR_cal( kijun_men_sum, kenri_men_sum ) if kijun_men_sum > 0	#2016/2/21
p	genbuR = "" if genbuR.nil?
  genbuR2 = genbuR_cal( kijun_men_sum, kanchi_men_sum ) if kanchi_men_sum > 0	#2016/2/21
 
#p genbuR if fcode == '09018000'
	h_1n[ fcode ] = [ kenri_men_sum.to_s, genbuR, genbuR2 ]
	return type
end

def kenri_men_cal( kenri_sisu, kan_m2_sisu )
	wk = kenri_sisu.to_f / kan_m2_sisu.to_f
	wk_s = wk.to_s
	if wk_s =~ /^(\d+\.\d\d)\d*/
		return $1.to_f
	else
		return wk_s.to_f
	end
end

def genbuR_cal( kijun_men, kenri_men )
	return "" if kijun_men.nil? || kijun_men == 0
	sprintf( "%0.2f%", 100.0 - 100.0 * ( kenri_men.to_f / kijun_men.to_f ) )
end

def h_1n_bun h_1n, fcode, y
  key = fcode + "bun"
	h_1n[ key ] = [] if h_1n[key].nil?
	bun = y.chiban[/[a-z]$/]
	h_1n[ key ].push( [ bun, y.block, y.lot ])
end

#############################################
def printRec_edit( arr, h_1n )
	uRec = OneUnitRec.new
	uRec.sort_key = arr[0].fcode
	if arr.size == 1
		a = arr[ 0 ]
		edit_zenRec( a, uRec )
		edit_hyokaRec( a, uRec )
		edit_gokei( a, uRec, h_1n )
		return uRec
	end
	hantei = nil
	arr.each{ | a |
		hantei = rec_hantei( a )
		case hantei
			when 'tandoku'
				print "hantei error"; exit
			when 'zen'
				flg_status = 'zen'
				edit_zenRec( a, uRec )
			when 'hyo'
				edit_hyokaRec( a, uRec )
				if a.chiban =~ /.+([a-j])$/
					uRec.sort_key += $1
					edit_gokei( a, uRec, h_1n )
				end
			when 'gokei'
				edit_hyokaRec( a, uRec ) if a.rosen.to_s != ''
				edit_gokei( a, uRec, h_1n )
			else  print "error in printRec_edit";p a;exit
		end
	}
	return uRec
end

def rec_hantei( a )
#p a.sho_code
	#for horyuchi
	return 'gokei' if a.sho_code == '5000' and a.fcode.nil?
#	return 'tandoku' if a.sho_code == '5000' and a.hyoka_kubun.to_s == ''
#	return 'zen' if a.fcode =‾ /^50....../

	return 'tandoku' if a.fcode and a.block.to_s != ''
	return 'zen'     if a.fcode and a.hyoka_men.to_s == ''
	return 'hyo'		if a.fcode.nil? and a.toki_men.to_s == ''
	return 'gokei'	if a.fcode.nil? and a.toki_men.to_s != ''
	print "rec_hantei error";p a.sort;exit
end

def edit_zenRec( a, uRec )
	z = ZenRecKan.new
	z.chomei = a.azaname
	z.chiban = a.chiban
	z.tokiChimoku = a.chimoku
	z.genkyoChimoku = ''
	z.tokiMen = a.toki_men
	z.kijunMen = format("%1.2f", a.kijun_men.to_f)
	z.heibeiSisu = a.zen_m2_sisu
	z.hyokaSisu = a.zen_hyo_sisu
	z.kenriSisu = a.zen_ken_sisu
	z.fudecode = a.fcode
	z.sho_code = a.sho_code
	z.shimei = a.shimei
	uRec.arrZenRec.push( z )
end

def edit_hyokaRec( a, uRec )
	return if uRec.h_HyokaRec.size > 0 and (a.hyoka_kubun == "" || a.hyoka_kubun.nil?)			#2016/3/26
	r = HyokaRec.new
	r.wk_heibei_sisu = a.heibei_sisu
	r.fugo = a.hyoka_kubun
	r.fugo = '' if r.fugo.nil?
	r.men = a.hyoka_men
	r.rosen = a.rosen
	r.rosenka = a.rosenka
	r.sho_heibei_sisu = a.hy_shomen
	r.hyoka_sisuA = a.hy_sisu

	r.arr_shusei.push( [ ( "奥行" + a.oku ), a.okuR ] )	if a.oku.to_s != ''
#	r.arr_shusei.push( [("整備度" + a.seibido ), a.seibidoR ] ) if a.seibido.to_s != ''
	r.arr_shusei.push( [( "長大" + a.chodai ), a.chodaiR ])  if a.chodai.to_i > 0
	r.arr_shusei.push( [("間口" + a.maguchi_kyosho ), a.maguchiR ] ) if
		 a.maguchi_kyosho.to_s != ''
	r.arr_shusei.push( [( "高低 " + a.kotei ), a.koteiR ] ) if a.kotei.to_s != ''
	r.arr_shusei.push( [( "水路" + a.suiro ), a.suiroR ] ) if a.suiro.to_s != ''
	r.arr_shusei.push( [( "三角" + a.san_fus_men ), a.san_fusR ] ) if
		a.san_fus_men.to_s != ''
	r.arr_shusei.push( [( "島地" + a.simachi ), a.simachiR ] ) if a.simachi.to_s != ''
	r.arr_shusei.push( [( "袋地" + a.fukuro ), a.fukuroR ] ) if a.fukuro.to_s != ''
	rate, news = @hoko.num2news[ a.houi ]
	r.arr_shusei.push( [( "方位 " + news ), a.houiR ] ) if a.houi.to_s != ''
	#2016/3/26 edit next line
	r.arr_shusei.push( [( "高・水" + a.koatu ), a.koatuR ] ) if a.koatu.to_s != ''
#	r.arr_shusei.push( [( "私道" + a.sido ), a.sidoR ] ) if a.sido.to_s != ''
	edit_sok_haiRec( a, r )
	uRec.h_HyokaRec[ r.fugo ] = r

end

def edit_hyokaRec2( a, uRec )
	fugo = a.hyoka_kubun
	r = uRec.h_HyokaRec[ fugo ]
	edit_sok_haiRec( a, r )
end

def edit_sok_haiRec( a, r )
	s = nil
	if a.sk1_keitai.to_s != ''
		s = SokuHaiRec.new
		s.keitai = a.sk1_keitai
		s.rosen = a.sk1_rosen
		s.rosenka = a.sk1_rosenka
		s.kasanR = a.sk1_kasanR
		s.len = a.sk1_len
		s.hyoka_sisuB = a.sk1_hyoka
		
		s.arr_shusei.push( [( "高低" + a.sk1_kotei ), a.sk1_koteiR ] ) if
				 a.sk1_kotei.to_s != ''
		s.arr_shusei.push( [( "水路" + a.sk1_suiro ), a.sk1_suiroR ] ) if 
				a.sk1_suiro.to_s != ''
		r.arr_soku.push( s )
	end
	if a.sk2_keitai.to_s != ''
		s = SokuHaiRec.new
		s.keitai = a.sk2_keitai
		s.rosen = a.sk2_rosen
		s.rosenka = a.sk2_rosenka
		s.kasanR = a.sk2_kasanR
		s.len = a.sk2_len
		s.hyoka_sisuB = a.sk2_hyoka
		s.arr_shusei.push( [( "高低" + a.sk1_kotei ), a.sk2_koteiR ] ) if
				 a.sk1_kotei.to_s != ''
		s.arr_shusei.push( [( "水路" + a.sk1_suiro ), a.sk2_suiroR ] ) if 
				a.sk1_suiro.to_s != ''
		r.arr_soku.push( s )
	end
end

def edit_gokei( a, uRec, h_1n )
	r = KanGokeiRec.new
	r.toki_men = a.toki_men
	r.kijun_men = a.kijun_men
	r.kenri_sisu = a.zen_ken_sisu
	r.men = a.hyoka_men
	r.hyoka_sisuA = a.hy_sisu
	r.hyoka_sisuB = ( a.sk1_hyoka.to_i + a.sk2_hyoka.to_i ).to_s
	r.hyoka_sisuSum = a.gokei_sisu
	r.heibei_sisu = a.kan_m2_sisu
	r.block_num = a.block
	r.lot_num = a.lot
	r.kan_men = a.kan_men
	r.kan_hyoka_sisu = a.kan_hyoka_sisu
	r.kan_genbuR = a.genbuR
	r.choshu_sisu = a.choshu
	r.kofu_sisu = a.kofu
	if uRec.sort_key =~ /^(........)(.)$/
		dum, r.kenri_genbuR, r.kan_genbuR = h_1n[ $1 ]   #2016/3/26
		r.kenri_men, dum = h_1n[ uRec.sort_key ]
	else
		r.kenri_men, r.kenri_genbuR, r.kan_genbuR = h_1n[ uRec.sort_key ]  #2016/3/26
	end
	uRec.gokeiRec = r
end
end #end of module

class HokoConv
	attr_reader :num2news
	def initialize
	@num2news = { "1"=>[ 1.030, "S" ], "2"=>[ 1.000, "E" ], "3"=>[ 1.000,"W" ],
				"4"=>[ 0.970, "N" ], "5"=>[ 1.015,"SE" ], "6"=>[ 1.015, "SW" ],
				"7"=>[ 0.985, "NE" ], "8"=>[ 0.985, "NW" ] }
	end
end