module KanchiTextGen
  def kanchi_text_gen f_code
    str = ""
    if f_code[/^50/]
      a = Kanchi.find_by(f_code: f_code)
      arr_obj = [ YAML.load(a.tmp_text) ]
      str += "<>保留地********************\n筆コード:#{f_code}"
    else
      land = Land.find_by( f_code: f_code)
#    arr_obj = land.kanchis
      arr_obj = land.kanchis.map do |a| YAML.load(a.tmp_text) end
    end

    arr_obj.sort{|a,b| a.sort_key <=> b.sort_key }.each do |obj|
      str += kan_bunkatu obj
    end
    str
  end

  def kan_bunkatu obj
    line = "  " + "*"*30
#    kan_hdr = ">>換地-------"
    if obj.sort_key[/([a-z])$/]
#      str = format("<>換地符号:%s#{line}\n分割面積:%8.2f\n権利指数:%8d\n",
#          $1, obj.gokeiRec.kijun_men.to_f, obj.gokeiRec.kenri_sisu.to_i)
      str = format("<>換地符号:%s#{line}\n分割面積:%8.2f\n",
          $1, obj.gokeiRec.kijun_men.to_f)
      str += kan_hyoka obj
      return str
    else
      return "\n<>換地符号:-#{line}\n" + kan_hyoka(obj)
    end
  end
  
  def kan_hyoka obj
    gk = obj.gokeiRec
#    str = format("街区:%s\n画地:%s\n面積:%3.2f\n",
#        gk.block_num, gk.lot_num, gk.kan_men.to_f)
    str = format("街区:%s\n画地:%s\n",
        gk.block_num, gk.lot_num)
#    @bl_lt = gk.block_num + "BL" + gk.lot_num  #20160709
    if gk.kento and gk.kento.size > 0
      str += ("検討:" + gk.kento + "\n")
    end
    unless gk.arrMemo.nil?
      gk.arrMemo.each do |s|
        str += ("メモ:" + s + "\n")
      end
    end
    obj.h_HyokaRec.keys.sort.each do |key|
      hy = obj.h_HyokaRec[key]
      fugo = hy.fugo
      fugo = "-" if fugo == ""
      str += format("=>評価符号:%s   #-,1,2  :-は分割なし\n", fugo )
      str += format("評価地積:%7.2f\n",hy.men.to_f)
      str += format("路線番号:%8s\n",hy.rosen)
      str += KanHyoka.shusei_arr_to_text hy.arr_shusei
      str += KanHyoka.sokuhai_arr_to_text hy.arr_soku
      str += "\n"
    end
    str
  end
  

end #end of module