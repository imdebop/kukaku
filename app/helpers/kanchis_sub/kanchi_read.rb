#
#
#
#2004/04/28
#2004/07/14

require_dependency 'kanchis_sub/ex_read_kan'
#require 'clipbrd'  #20160215 test code ###########

class ExRead
  include Ex_read
  def initialize
	@hoko = HokoConv.new
	@fl_status = "0"
	@fl_rec = "blank"
  end

  def ex_get str
	@a = KanchiExcelRec.new(nil)
#	arr_wk_all = IO.readlines("kanchi_data.txt") 
#	arr_wk_all = Clipboard.GetText + "\n"
	arr_wk_all = str.split(/\n/)
	arr_wk = []
	h_1n = {}

	#delete blank lines
	arr_wk_all.each{ | x |
	  next if x[/^#/]						#2016/3/26
		res = @a.str2obj( x )
		arr_wk.push( @a.dup ) if res
	 }

	flg_cont = nil
	arr2_pre = []
	arr_pre = []
	arr_wk.each{ | x |
#p x
#p rec_hantei( x )
#p x.sho_code
		if rec_hantei( x ) == 'tandoku'
			arr2_pre.push( arr_pre ) if arr_pre.size > 0
			arr_pre = []
			arr2_pre.push( [ x ] )
			flg_cont = nil
		elsif x.fcode and flg_cont.nil?
			arr2_pre.push( arr_pre ) if arr_pre.size > 0
			arr_pre = []
			arr_pre.push( x )
			flg_cont = true
		elsif x.fcode and flg_cont
			arr_pre.push( x )
		elsif x.fcode.nil?
			arr_pre.push( x )
			flg_cont = nil
		else
			print "sequence error";exit
		end
	}
	#last rec
	if arr_pre.size > 0
		arr2_pre.push( arr_pre )
	end

	#delete horyuchi data
	arr2_pre.delete_if{ | x |
		if x[0].sho_code == '5000'
print "horyuchi deleted\n" ##### test code ########
			true
		else
			nil
		end
	} if nil #### test code #####

	arr = []
	arr2_pre.each{ |x|
		if x.size == 1
			arr.push( x )
			kanchi_type = kanchi_type_chk( x, h_1n )
			next
		end

		kanchi_type = kanchi_type_chk( x, h_1n )

		#1:n���n�̎��A[�]�O�n+n���n]�̃��R�[�h������
		if kanchi_type == '1n'
			wk_top = x.shift
			arr_wk = [ wk_top.dup ]
			x.each{| a |
				arr_wk.push( a )
				if a.chiban.to_s != ''
					arr.push( arr_wk.dup )
					arr_wk = [ wk_top.dup ]
				end
			}
		else
			#n:1�͂��̂܂܍s���Ƀv�b�V��
			arr.push( x )
		end
	}
	#���n���ɐ؂蕪�������R�[�h�̕ҏW
	arr_res = []
	arr.each{ | x |
		oneUnit = printRec_edit( x, h_1n )
#p h_1n[ '09018000' ] if x[0].fcode == '09018000'
#p h_1n[ oneUnit.sort_key ] if x[0].fcode == '09018000'
		keys = oneUnit.h_HyokaRec.keys
		if keys.size == 1
			key = keys[ 0 ]
			if oneUnit.h_HyokaRec[ key ].men == ''
				oneUnit.h_HyokaRec[ key ].men = oneUnit.gokeiRec.kan_men
				oneUnit.gokeiRec.men = oneUnit.gokeiRec.kan_men
			end
		end
		arr_res.push( oneUnit )
	}
	return [ arr_res, h_1n ]
 end

end



#require '../zen/zendata'

class KanchiExcelRec
  attr_accessor  :fcode,:azaname,:chiban,:chimoku,:toki_men,:kijun_men,
	:zen_m2_sisu,:zen_hyo_sisu,:zen_ken_sisu,
	:hyoka_kubun,:dum_bk_num,:dum_lot_num,:hyoka_men,
	:rosen,:rosenka,:oku,:okuR,:maguchi,:maguchi2,
	:chodai,:chodaiR,:maguchi_kyosho,:maguchiR,:kotei,:koteiR,:suiro,:suiroR,
	:san_fus_men,:san_fusR,:simachi,:simachiR,:fukuro,:fukuroR,:houi,:houiR,
	:koatu,:koatuR,:hy_shomen,:hy_sisu,
	:sk1_keitai,:sk1_rosen,:sk1_rosenka,:sk1_len,:dummy,:sk1_kasanR,
	:sk1_kotei,:sk1_koteiR,:sk1_suiro,:sk1_suiroR,:sk1_hyoka,
	:sk2_keitai,:sk2_rosen,:sk2_rosenka,:sk2_len,:dummy,:sk2_kasanR,
	:dummuy,:sk2_koteiR,:dummy,:sk2_suiroR,:sk2_hyoka,
	:gokei_sisu,:heibei_sisu,:hyotei_sisu,
	:dum_num,:block,:lot,:kan_m2_sisu,:kan_men,:kan_hyoka_sisu,
	:choshu,:kofu,:genbuR,
	:sho_code,:shimei

  def initialize(dummy)
	@initVal = "\t" * 80
	recSet(@initVal)
	return nil
  end

  def str2obj(str)
	if str =~ /^\t+$/
		return nil 
	end
	recSet(str)
	if @fcode == ""
		@fcode = nil
	elsif @fcode.size == 7
		@fcode = "0" + @fcode
	end
#	if @sho_code == '5000'
#		return nil
#	end
	return true
  end

  def recSet(str)
	@fcode,@azaname,@chiban,@chimoku,@toki_men,@kijun_men,
	@zen_m2_sisu,@zen_hyo_sisu,@zen_ken_sisu,
	@hyoka_kubun,dum_bk_nuum,dum_lot_num,@hyoka_men,
	@rosen,@rosenka,@oku,@okuR,@maguchi,@maguchi2,
	@chodai,@chodaiR,@maguchi_kyosho,@maguchiR,@kotei,@koteiR,@suiro,@suiroR,
	@san_fus_men,@san_fusR,@simachi,@simachiR,@fukuro,@fukuroR,@houi,@houiR,
	@koatu,@koatuR,@hy_shomen,@hy_sisu,
	@sk1_keitai,@sk1_rosen,@sk1_rosenka,@sk1_len,dummy,@sk1_kasanR,
	@sk1_kotei,@sk1_koteiR,@sk1_suiro,@sk1_suiroR,@sk1_hyoka,
	@sk2_keitai,@sk2_rosen,@sk2_rosenka,@sk2_len,dummy,@sk2_kasanR,
	dummuy,@sk2_koteiR,dummy,@sk2_suiroR,@sk2_hyoka,
	@gokei_sisu,@heibei_sisu,@hyotei_sisu,
	dum_num,@block,@lot,@kan_m2_sisu,@kan_men,@kan_hyoka_sisu,
	@choshu,@kofu,@genbuR,
	@sho_code,@shimei = str.split("\t")
  end


end




