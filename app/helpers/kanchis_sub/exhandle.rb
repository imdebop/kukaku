require 'spreadsheet'
Spreadsheet.client_encoding = 'UTF-8'

class ExHandle
  include Spreadsheet
  def initialize
    @r_path = Rails.root.to_s
    @new_book = Spreadsheet.open("#{@r_path}/app/helpers/kanchis_sub/keisansho.xls")
    @sheet = @new_book.worksheet(0)
    self
  end

  def Range addr
    if addr[/(\w+):(\w+)/]
      @f_cell = cell_addr_str_to_num( $1 )
      @l_cell = cell_addr_str_to_num( $2 )
      @f_row = @f_cell[0].to_i
      @l_row = @l_cell[0].to_i
      @rows = @l_row - @f_row + 1
      @f_col = @f_cell[1].to_i
      @l_col = @l_cell[1].to_i
      @cols = @l_col - @f_col + 1
      if @f_cell == @l_cell
        @rc = cell_addr_str_to_num( addr )
      else 
        @rc = nil
      end
    else
      @rc = cell_addr_str_to_num( addr )
    end
    self
  end

  def value= ( val ) 
    if val.size == 1
      wk_val = val.pop
    else
      wk_val = val
    end
    if @rc
      @sheet[@rc[0], @rc[1]] = wk_val
    elsif @rows == 1
      n = 0
      @f_col.upto(@l_col).each do |c|
        @sheet[ @f_row, c ] = wk_val[n]
        n += 1
      end
    end 
    true
  end

  def ClearContents
    @f_row.upto(@l_row).each do |r|
      @f_col.upto(@l_col).each do |c|
        @sheet[ r, c ] = "" 
      end
    end
  end

  def cell_addr_str_to_num(cell_addr)
    #convert cell's address like "B3" -> "r3c2"
    cell_addr.upcase!
    r,c = [cell_addr[/\d+/].to_i,
	cell_addr[/[A-Z]+/].chars.inject(0){|a,b|a*26+('A'..'Z').find_index(b)+1}]
    [ r - 1, c - 1 ] 
  end

  def write fname
#p Rails.root
    @new_book.write("#{@r_path}/kukaku_data/#{fname}.xls")
  end
  
  def close
    @new_book.io.close
  end
end


#new_keisan = ExHandler.new

#new_keisan.Range("f5").value = ( "fjfjfjfjfjfjf" )
#new_keisan.write

#open_book = Spreadsheet.open('text.xls')


