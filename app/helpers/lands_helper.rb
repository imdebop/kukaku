module LandsHelper
  def recalc fdata
    hyotei_sisu =  ( fdata.zen_m2sisu.to_f * fdata.kijun_men.to_f ).round
    kenri_sisu = (hyotei_sisu * 1.006).round
    fdata.update(hyotei_sisu: hyotei_sisu.to_s, kenri_sisu: kenri_sisu.to_s)
  end



end  #end of module
