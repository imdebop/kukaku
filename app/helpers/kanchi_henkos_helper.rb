require_dependency 'kanchis_sub/kanchi_preview'
require_dependency 'kanchis_sub/kanchi_text_gen'
require_dependency 'kanchis_sub/kanchi_text_conv'
module KanchiHenkosHelper
  include KanchiPreview
  include KanchiTextGen
  include KanchiTextConv

  def kanchi_henko_preview f_code
    if f_code[/^50/]
      land = Land.new
      land.horyu = f_code
      str = "*** 保留地 ***\n" + kan_preview( land )
      return str
    end

    land, str = zen_preview(f_code)
    if land.nil?
      str + "\n換地データがありません\n"
    else
      str + kan_preview( land )
    end
  end

  def kanchi_henko_text f_code
    str = ""
    if f_code[/^50/]
    else
      land = Land.find_by( f_code: f_code)
      if land.nil?
        return "換地データがありません\n"
      end
      str = new_hdr + "筆コード:#{f_code}\n"
      if land.children.size > 0
        land.children.each do |a|
        str += "筆コード:#{a.f_code}\n"
        end
      end
    end
    str += kanchi_text_gen f_code
    str
  end

  def kanchi_text_preview details, action
    @arr_oneRec, msg = text_conv details
    f_code = @arr_oneRec[0].arrZenRec[0].fudecode
    if msg
      str = msg + "\n"
    else
      str = ""
    end
    str += kanchi_henko_preview f_code
    [str, @arr_oneRec]
  end

  def new_hdr
str =  <<"EOS"
############## 換地変更データ ################
<>--従前地-----
EOS
  end

  def new_text
str =  <<"EOS"
#{new_hdr}
筆コード:09029000(保留地:h)
<>換地符号:a  ******************************
分割面積: 1005.41
街区:22
画地:6
検討:～任意（赤字で記載）
メモ:～任意（複数行可)
=>評価符号:-   #-,1,2  :-は分割なし
評価地積: 706.61
路線番号: 06-55-1
奥行:025(袋地:025 〜奥行は不要)
方位:NW
->側面・背面加算--
  形態    :+
  路線番号:06-56
  延長    :25.2

<>換地符号:b  ******************************
分割面積:   67.14
街区:39
画地:65
=>評価符号:-   #-,1,2  :-は分割なし
評価地積:  38.02
路線番号:   16-17
奥行:019
方位:E
->側面・背面加算--
  形態    :+
  路線番号:T6-3
  延長    :0.8
->側面・背面加算--
  形態    :+
  路線番号:11.5-1-5
  延長    :0.8
->側面・背面加算--
  形態    :=
  路線番号:06-12-4
  延長    :2.0

<>換地符号:c  ******************************
分割面積:  342.41
街区:24
画地:11
=>評価符号:-   #-,1,2  :-は分割なし
評価地積: 237.63
路線番号: 06-55-1
奥行:023
方位:SE


筆コード:XXXXXXXX
筆コード:XXXXXXXX #多対１の時筆コードは複数。最初の筆が代表となる
分割符号:X  #１対多の時, a,b,c・・・(アルファベット小文字１字)
分割面積:XXXX.XX

>>換地-------
符号:X  #0は分割なし。分割評価の時、1,2,3・・・ 　として繰り返す　不要項目は削除可
街区:XX           #必須
ロット:XX         #必須  枝番可  XX-XX
評価地積:XXXX.XX  #必須
正面路線:XXXX     #必須
奥行    :XX       #必須 m単位
間口    :XX.X     #4m未満の時、側方・背面加算、または奥行長大修正が必要な時
高低差  :X.X      #マイナス符号可
水路    :X.X
三角　  :XX.XXX   #三角地・不整形地　角度.割合
袋地    :X.XX     #該当地は 0.95
方位    :XX       #数字、英字ともに可 1/S,2/E,3/W,4/N,5/SE,6/SW,7/NE,8/NW
高・水  :X.X      #高圧線下、私道、水路敷の修正係数


    --側方・背面加算---- 各評価単位(符号)毎に２つまで。それ以上は別符号で計算
    形態    :X    #必須  側面（＋，Ｔ，Ｌ）背面（＝，－）
    路線    :XXXX #必須
    延長    :XX.X #側方加算の時必須
    高低差  :X.X
    水路    :X.X
-- 字コード --
01:牟呂町字大塚
02:牟呂町字奥山
03:牟呂町字奥山新田
04:牟呂町字北汐田
05:牟呂町字古田
06:牟呂町字古幡焼
07:牟呂町字中西
08:牟呂町字東里
09:牟呂町字百間
10:牟呂町字松崎
11:牟呂町字松島
12:牟呂町字松島東
13:牟呂町字松東
14:牟呂町字南汐田
19:神野新田町字会所前
21:柱四番町
22:柱五番町
24:藤沢町
25:潮崎町
EOS
  end

  def pre_view_init
    "*************  プレビュー  *****************"
  end

end
