require 'kanchis_sub/kanchi_kyotu'
require_dependency 'kanchis_sub/kanchi_read'
require 'spreadsheet'
require_dependency 'kanchis_sub/keisansho'
require 'yaml'
require 'pp'

module KanchisHelper	#換地変更と共用
	include Keisansho
	include KanchiKyotu
  def save_jikko kanAR, str, arrRec
		h_1n = nil
		if arrRec.nil?
	  	a = ExRead.new
		 	arrRec, h_1n = a.ex_get(str)
		end
	  arrTempData = []

    arrRec.each do | oneRec |			#2016/3/26
			res = nil
  	  kan_code = lot2code( oneRec.gokeiRec.block_num, oneRec.gokeiRec.lot_num)
			if h_1n
	    	oneRec.gokeiRec.h_1n = h_1n_filter( h_1n, oneRec.arrZenRec[0].fudecode )
			end
			if 	@kanchi = Kanchi.find_by( kanchi_code: kan_code )
				@kanchi.update( param_set(oneRec) )
#p "kanchi updated #{kan_code}"
			else
				kanAR=Kanchi.new
				kanAR.kanchi_code = kan_code
	  	  kanAR.f_code = oneRec.arrZenRec[0].fudecode
				kanAR.tmp_text = oneRec.to_yaml
				kanAR.save
#p "kanchi created #{kan_code}"
			end
		end
		land_parent_child arrRec[0] if arrRec[0].arrZenRec[0].sho_code != '5000'
		jikko arrRec
  end

	def param_set oneRec
		h={}
		h[:f_code] = oneRec.arrZenRec[0].fudecode
		h[:tmp_text] = oneRec.to_yaml
		h
	end

	def land_parent_child oneRec
		arr_fude = oneRec.arrZenRec.map do |r| r.fudecode end
		fcode_p = arr_fude.shift
		land = Land.find_by!(f_code: fcode_p)
		land.update( f_code_parent: 'parent' )
		arr_fude.each do |fcode|
			land = Land.find_by!(f_code: fcode)
			land.update( f_code_parent: fcode_p)
		end
	end

  def h_1n_filter h_1n, fcode
    h = {}
		h_1n.each do |key, val|
			next if key.nil?
			h[key] = val if key[/^#{fcode}/]
	  end
	  h
  end

	def lot2code blk, lot_full
		lot, eda = lot_full.split(/-/)
		eda = 0 if eda.nil?
		wk = format("%03d%03d%02d", blk, lot, eda)
	end

end

#2016/4/2 Moved to kanchi_kyotu.rb
#def kCode_to_kakuchiNo kanchi_code
#	p kanchi_code
#	kanchi_code[/^(...)(...)(..)/]
#  return kanchi_code if $1.nil? or $2.nil?
#	str = sprintf("%3dBL% 3dL ", $1.to_i, $2.to_i)
#  str += "-" + format("% 2d",$3.to_i) if $3.to_i > 0
#  str
#end
