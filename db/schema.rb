# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160404065315) do

  create_table "kanchi_henkos", force: :cascade do |t|
    t.string   "f_code"
    t.text     "details"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "kihon_codes", force: :cascade do |t|
    t.string   "type"
    t.string   "code"
    t.string   "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lands", force: :cascade do |t|
    t.string   "f_code"
    t.string   "chimoku"
    t.string   "gen_chimoku"
    t.decimal  "toki_men",    precision: 8, scale: 2
    t.decimal  "kijun_men",   precision: 8, scale: 2
    t.string   "owner_id"
    t.integer  "m2sisu"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "owners", force: :cascade do |t|
    t.string   "sho_code"
    t.string   "shimei"
    t.text     "jusho"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
